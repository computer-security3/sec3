# Cybersecurity 3 - Romain Absil (rabsil@he2b.be)

## Build

Note that you need an updated full LaTeX distribution (such as the one provided by texlive) as well as latexmk to build the slides.


### Make from within a chapter directory
#### Build while editing
To build and preview the slides with an automatically triggered rebuild of the slides when one of the dependant files changes:
```
make preview
```

Note that this requires xdg-open to be properly configured.

#### Build directly
Make the handouts and the slides:
```
make
```

Make the slides:
```
make slides
```

Make the handouts:
```
make handout
```

### Make from top-level directory

#### Build a chapter
Make chapter 4, [everything/handouts/slides/clean]:
```
make [ch4|ch4-handout|ch4-slides|ch4-clean]
```

#### Build everything

Make all the handouts using 4 build processes:
```
make -j4 [handouts|slides]
```

Make *everything*, slides and handout:
```
make -j4
```

#### Clean everything
```
make clean
```

## Other

Prepare a directory for a new course's chapter
```
make new_chapter
```

## License

This work is under Attribution-NonCommercial-ShareAlike 4.0 (CC BY-NC-SA 4.0).

Full legal code can be found on [Creative Commons](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode)
