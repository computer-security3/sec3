def getBalance():
    return 42  #academic : should be a remote call

def saveBalance():
    print "Balance saved" #academic : should be a remote call

def withdraw(amount):
    balance = getBalance()
    if amount <= balance:
        balance -= amount
        print amount, "$ withdrawn"
        saveBalance()
    else:
        print "Insufficient funds"
        
withdraw(10)