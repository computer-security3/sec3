\input{../common/header.tex}
\input{../common/cmds.tex}
\input{../common/crypto.tex}

\title{Ch. 1 - Introduction}

\begin{document}
\input{../common/front.tex}

\begin{frame}
\frametitle{Before we start}
\begin{itemize}[<+->]
\item Since WW2, there is a necessity to protect computer systems
	\begin{itemize}
	\item Different types of threat
	\item Different types of protection
	\end{itemize}
\end{itemize}
\begin{alertblock}<+->{Problem}
	\begin{itemize}[<+->]
	\item Strong security is heavier
	\end{itemize}
\end{alertblock}
\begin{exampleblock}<+->{Solution}
	\begin{itemize}[<+->]
	\item Analyse each risk and its probability to occur, and adapt your strategy
	\end{itemize}
\end{exampleblock}
\end{frame}

\begin{frame}
\frametitle{Types of risks}
\begin{itemize}[<+->]
\item Different types of risks
	\begin{itemize}
	\item Access a restricted resource or service
	\item Usurp somebody else's identity
	\item Access confidential data
	\item Falsification
	\end{itemize}
\item In this course, \emph{only} these risks are covered
\item For each of those risks
	\begin{itemize}
	\item Several possible attacks
	\item Several counter-measures available
	\end{itemize}
\end{itemize}
\end{frame}

\section{Objectives}

\begin{frame}
\frametitle{"When in doubt, use brute force"}
\begin{itemize}[<+->]
\item Many security systems rely on hard to solve mathematical problems
	\begin{itemize}
	\item Hard: takes a "considerable" amount of time without the proper codes
	\end{itemize}
\end{itemize}
\begin{exampleblock}<+->{Example}
	\begin{itemize}[<+->]
	\item Big numbers factorisation
	\item Invert functions
	\item Combinatorial optimisation
	\item Stochastic optimisation
	\end{itemize}
\end{exampleblock}
\begin{itemize}[<+->]
\item We're interested in this type of constraints
	\begin{itemize}
	\item Maths left aside
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Objectives}
\begin{enumerate}[<+->]
\item Availability: make sure a service is available for authorized users
\item Authentication: make sure only authorised users have access to restricted resources and services
\item Non repudiation: make sure a contract between $x$ and $y$ \emph{really} comes from $x$ and is \emph{really} being concluded with $y$
\item Confidentiality: make sure confidential data are confidential for anyone not authorised
\item Integrity: make sure data are left unaltered during transit
\end{enumerate}
\end{frame}

\subsection{Kerckhoff's principles}

\begin{frame}
\frametitle{Origin}
\begin{itemize}[<+->]
\item Introduced in 1883 by Auguste Kerckhoff, in military context
\end{itemize}
\begin{exampleblock}<+->{Principles}
	\begin{enumerate}[<+->]
	\item The system must be materially and mathematically unbreakable without the key
	\item The system must not require any secret (beyond the key), and must not be compromised should it fall into enemy hands
	\item The system must be "easy" to use
	\end{enumerate}
\end{exampleblock}
\begin{itemize}[<+->]
\item Security \emph{cannot} rely on secrecy
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{The context of cybersecurity}
\begin{exampleblock}<+->{Summary}
	\begin{itemize}[<+->]
	\item The opponent \emph{knows} the cryptographic / security system
	\end{itemize}
\end{exampleblock}
\begin{alertblock}<+->{Consequence}
	\begin{itemize}[<+->]
	\item A system's security must rely on the key (or access codes) \emph{only}, not on the system itself
	\end{itemize}
\end{alertblock}
\begin{itemize}[<+->]
\item It is hard (if not impossible) to bypass security without the proper codes
\item These codes must be easily modified
\item The security system must be analysed by experts
\end{itemize}
\end{frame}

\section{Cryptographic tools}

\subsection{Hash functions}

\begin{frame}
\frametitle{Cryptographic hash functions}
\begin{exampleblock}<+->{Definition}
\begin{itemize}[<+->]
\item Let $f : M \mapsto \IN$, $f$ is a cryptographic hash function if
	\begin{enumerate}
	\item computing $f(x)$ is easy for each $x\in M$
	\item $f$ is resistant to pre-image
		\begin{itemize}
		\item it is hard to invert $f$,
		\end{itemize}
	\item $f$ is resistant to second pre-image
		\begin{itemize}
		\item it is hard to change a message without its hash
		\end{itemize}
	\item $f$ is resistant to collisions
		\begin{itemize}
		\item it is hard to generate two different messages with the same hash
		\end{itemize}
	\end{enumerate}
\end{itemize}
\end{exampleblock}
\begin{itemize}[<+->]
\item Hash functions are also public
    \begin{itemize}
    \item No secret involved, such as a key
    \end{itemize}
\item The output as fixed (bounded) size
    \begin{itemize}
    \item Makes practical uses easy
    \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Illustration: checking integrity}
\begin{center}
\scalebox{.6}{
\begin{tikzpicture}
\onslide<2->{\node[draw,fill=blue!30] (plain) at (0,3) {"Hello Bob !"};}
\onslide<5->{\node[draw, inner sep = 20pt,fill=yellow!50] (cipher) at (5,3) {\tt Hash function};}
\onslide<4->{\draw[->,>=stealth] (plain) -- (cipher);}

\onslide<7->{\node[draw, fill=cyan!80, label={right:\small Hash}, text width=2.3cm, align=center] (ciphered) at (5.1,0) {\tt 6B 9C 64 AF\\23 F1 C4 D2\\3E 65 12 1A};}

\onslide<6->{\draw[->,>=stealth] (cipher) -- (5,0.8);}

\onslide<1->{\draw[very thick] (-2,-2) -- (11,-2);}

\onslide<9->{\node[draw, inner sep = 20pt,fill=yellow!50] (uncipher) at (5,-5) {\tt Check};}
\onslide<8->{\draw[->,>=stealth,very thick, dotted] (5,-0.8) -- (uncipher) node[midway, above, sloped, xshift=10pt] {Send};}
\onslide<3->{\draw[->,>=stealth,very thick, dotted] (plain.east) to[out=0,in=90]  node[pos=.5,below,sloped] {Send} (2.75,0) to[out=-90,in=90] (uncipher.north);}

\visible<11->{\node (ok) at (0,-5) {\includegraphics[width=1.5cm]{pics/ok.png}};}
\onslide<10->{\draw[->,>=stealth] (uncipher) -- (ok);}

%\onslide<6->{\draw[->,>=stealth] (8,3) -- (cipher);}
%\onslide<12->{\draw[->,>=stealth] (8,-5) -- (uncipher);}

\onslide<1->{\node at (-1,-0.5) {\LARGE Alice};}
\onslide<1->{\node at (-1,-3.5) {\LARGE Bob};}
\end{tikzpicture}}
\end{center}
\end{frame}

\subsection{Cipher algorithms}

\begin{frame}
\frametitle{Cipher algorithms}
\begin{exampleblock}<+->{Objective}
\begin{itemize}[<+->]
\item Encode messages with one or two ``keys''
	\begin{enumerate}
	\item Single key: symmetric cryptography
		\begin{itemize}
		\item This key is \emph{secret}
		\end{itemize}
	\item A key to encode (public), a key to decode (private): public key (asymmetric) cryptography
		\begin{itemize}
		\item Only the private key is secret
        \item The operation that needs to be protected is performed with the private key
        \item Encryption: we cipher with the public key, and decipher with the private one
		\end{itemize}
	\end{enumerate}
\end{itemize}
\end{exampleblock}
\begin{itemize}[<+->]
\item Without the (private) key, it is hard to recover the original message
    \begin{itemize}
    \item Or to deduce the private key from the public one
    \end{itemize}
\item Different from steganography
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Illustration: symmetric cipher}
\begin{center}
\scalebox{.7}{
    \begin{tikzpicture}
    \onslide<3->{\node[draw,fill=blue!30] (plain) at (0,3) {"Hello Bob!"};}
    \onslide<6->{\node[draw, inner sep = 20pt,fill=yellow!50] (cipher) at (5,3) {\tt Cipher};}
    \onslide<4->{\draw[->,>=stealth] (plain) -- (cipher);}

    \onslide<8->{\node[draw, fill=cyan!80, label={right:\small Ciphered text}, text width=2.3cm, align=center] (ciphered) at (5.1,0) {\tt 6B 9C 64 AF\\23 F1 C4 D2\\3E 65 12 1A};}

    \onslide<7->{\draw[->,>=stealth] (cipher) -- (5,0.8);}

    \begin{scope}[shift={(9,-1.85)},scale=.25]
    \onslide<2->{\labelledkey{Dandelion}{Secret Key}}
    \end{scope}

    \onslide<1->{\draw[very thick] (-2,-2) -- (7,-2);}

    \onslide<11->{\node[draw, inner sep = 20pt,fill=yellow!50] (uncipher) at (5,-5) {\tt Uncipher};}
    \onslide<9->{\draw[->,>=stealth,very thick, dotted] (5,-0.8) -- (uncipher) node[midway, above, sloped, xshift=10pt] {Send};}

    \onslide<13->{\node[draw,fill=blue!30] (clear) at (0,-5) {"Hello Bob!"};}
    \onslide<12->{\draw[->,>=stealth] (uncipher) -- (clear);}

    \onslide<5->{\draw[->,>=stealth, very thick] (9,-1) to[out=90,in=0] (cipher);}
    \onslide<10->{\draw[->,>=stealth, very thick] (9,-2.5) to[out=-90,in=0] (uncipher);}

    \onslide<1->{\node at (-1,-0.5) {\LARGE Alice};}
    \onslide<1->{\node at (-1,-3.5) {\LARGE Bob};}
    \end{tikzpicture}
}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Illustration: public key cipher}
\begin{center}
\scalebox{.7}{
\begin{tikzpicture}
\onslide<2->{\node[draw,fill=blue!30] (plain) at (0,3) {"Hello Bob!"};}
\onslide<5->{\node[draw, inner sep = 20pt,fill=yellow!50] (cipher) at (5,3) {\tt Cipher};}
\onslide<4->{\draw[->,>=stealth] (plain) -- (cipher);}

\onslide<7->{\node[draw, fill=cyan!80, label={right:\small Ciphered text}, text width=2.3cm, align=center] (ciphered) at (5.1,0) {\tt 6B 9C 64 AF\\23 F1 C4 D2\\3E 65 12 1A};}

\onslide<6->{\draw[->,>=stealth] (cipher) -- (5,0.8);}

\begin{scope}[shift={(9,3)},scale=.25]
\onslide<3->{\labelledkey{Green}{Public key of Bob}}
\end{scope}

\begin{scope}[shift={(9,-5)},scale=.25]
\onslide<9->{\labelledkey{red!80}{Private key of de Bob}}
\end{scope}

\onslide<1->{\draw[very thick] (-2,-2) -- (11,-2);}

\onslide<11->{\node[draw, inner sep = 20pt,fill=yellow!50] (uncipher) at (5,-5) {\tt Uncipher};}
\onslide<8->{\draw[->,>=stealth,very thick, dotted] (5,-0.8) -- (uncipher) node[midway, above, sloped, xshift=10pt] {Send};}

\onslide<13->{\node[draw,fill=blue!30] (clear) at (0,-5) {"Hello Bob!"};}
\onslide<12->{\draw[->,>=stealth] (uncipher) -- (clear);}

\onslide<4->{\draw[->,>=stealth] (8,3) -- (cipher);}
\onslide<10->{\draw[->,>=stealth] (8,-5) -- (uncipher);}

\onslide<1->{\node at (-1,-0.5) {\LARGE Alice};}
\onslide<1->{\node at (-1,-3.5) {\LARGE Bob};}
\end{tikzpicture}}
\end{center}
\end{frame}

\subsection{Signatures}

\begin{frame}
\frametitle{Signature algorithms}
\begin{itemize}[<+->]
\item Basic idea: make sure the sender is who we think he is
\item More sophisticated than a signature at the bottom of a page
	\begin{itemize}
	\item Usurp the identity of someone must be hard
	\item Principle: we sign ``through'' the whole document, imprinting data.
	\end{itemize}
\item Works with a combination of hash functions and ciphers
	\begin{itemize}
	\item We cipher the hash of the message with our private key
    \item The signature is authenticated with our public key
	\end{itemize}
\item Makes sure the message
	\begin{itemize}
	\item was left unaltered
	\item is genuine
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Illustration: signature}
\begin{center}
\scalebox{.6}{
\begin{tikzpicture}
\onslide<2->{\node[draw,fill=blue!30] (plain) at (0,3) {"Hello Bob !"};}
\onslide<7->{\node[draw, inner sep = 20pt,fill=yellow!50] (cipher) at (5,3) {\tt Sign};}
\onslide<4->{\draw[->,>=stealth] (plain) -- (cipher);}

\onslide<9->{\node[draw, fill=cyan!80, label={right:\small Numerical signature}, text width=2.3cm, align=center] (ciphered) at (5.1,0) {\tt 6B 9C 64 AF\\23 F1 C4 D2\\3E 65 12 1A};}

\onslide<8->{\draw[->,>=stealth] (cipher) -- (5,0.8);}

\begin{scope}[shift={(9,3)},scale=.25]
\onslide<5->{\labelledkey{red!80}{Private key of Alice}}
\end{scope}

\begin{scope}[shift={(9,-5)},scale=.25]
\onslide<11->{\labelledkey{Green}{Public key of Alice}}
\end{scope}

\onslide<1->{\draw[very thick] (-2,-2) -- (11,-2);}

\onslide<13->{\node[draw, inner sep = 20pt,fill=yellow!50] (uncipher) at (5,-5) {\tt Authenticate};}
\onslide<10->{\draw[->,>=stealth,very thick, dotted] (5,-0.8) -- (uncipher) node[midway, above, sloped, xshift=10pt] {Send};}
\onslide<3->{\draw[->,>=stealth,very thick, dotted] (plain.east) to[out=0,in=90]  node[pos=.5,below,sloped] {Send} (2.75,0) to[out=-90,in=90] (uncipher.north);}

\visible<15->{\node (ok) at (0,-5) {\includegraphics[width=1.5cm]{pics/ok.png}};}
\onslide<14->{\draw[->,>=stealth] (uncipher) -- (ok);}

\onslide<6->{\draw[->,>=stealth] (8,3) -- (cipher);}
\onslide<12->{\draw[->,>=stealth] (8,-5) -- (uncipher);}

\onslide<1->{\node at (-1,-0.5) {\LARGE Alice};}
\onslide<1->{\node at (-1,-3.5) {\LARGE Bob};}
\end{tikzpicture}}
\end{center}
\end{frame}

\section{Key exchange}

\begin{frame}
\frametitle{The context}
\begin{itemize}[<+->]
\item If two actors never met before, they have to exchange keys
    \begin{itemize}
    \item to encrypt sensitive information they send to each others
    \item to sign messages
    \end{itemize}
\item Symmetric cryptography is faster than public key cryptography
    \begin{itemize}
    \item We would rather use it
    \end{itemize}
\end{itemize}
\begin{alertblock}<+->{Problems}
    \begin{itemize}[<+->]
    \item Symmetric keys cannot be intercepted
    \item We don't have any information about the \emph{identity} of the owner of a public key
    \end{itemize}
\end{alertblock}
\end{frame}

\begin{frame}
\frametitle{Illustration: RSA key exchange}
\begin{center}
\scalebox{.6}{
\begin{tikzpicture}
\onslide<2->{\node[draw, inner sep = 10pt,fill=yellow!50, text width=3cm] (gen) at (-5,3) {Symmetric key generation};}
\onslide<3->{\draw[->,>=stealth] (gen) -- (-1.2,3);}

\onslide<4->{\draw[fill=blue!30] (-1.2,2.25) rectangle (1.8,3.75);}
\begin{scope}[shift={(0,3)},scale=.25]
\onslide<4->{\labelledkey{Dandelion}{Session key}}
\end{scope}

\onslide<7->{\node[draw, inner sep = 20pt,fill=yellow!50] (cipher) at (5,3) {\tt Cipher};}
\onslide<6->{\draw[->,>=stealth] (1.8,3) -- (cipher);}

\onslide<9->{\node[draw, fill=cyan!80, label={right:\small Ciphered key}, text width=2.3cm, align=center] (ciphered) at (5.1,0) {\tt 6B 9C 64 AF\\23 F1 C4 D2\\3E 65 12 1A};}

\onslide<8->{\draw[->,>=stealth] (cipher) -- (5,0.8);}

\begin{scope}[shift={(9,3)},scale=.25]
\onslide<5->{\labelledkey{Green}{Public key of Bob}}
\end{scope}

\begin{scope}[shift={(9,-5)},scale=.25]
\onslide<11->{\labelledkey{red!80}{Private key of Bob}}
\end{scope}

\onslide<1->{\draw[very thick] (-5,-2) -- (11,-2);}

\onslide<13->{\node[draw, inner sep = 20pt,fill=yellow!50] (uncipher) at (5,-5) {\tt Uncipher};}
\onslide<10->{\draw[->,>=stealth,very thick, dotted] (5,-0.8) -- (uncipher) node[midway, above, sloped, xshift=10pt] {Send};}

\onslide<15->{\draw[fill=blue!30] (-1.2,-4.25) rectangle (1.8,-5.75);}
\begin{scope}[shift={(0,-5)},scale=.25]
\onslide<15->{\labelledkey{Dandelion}{Session key}}
\end{scope}

\onslide<6->{\draw[->,>=stealth] (8,3) -- (cipher);}
\onslide<12->{\draw[->,>=stealth] (8,-5) -- (uncipher);}
\onslide<14->{\draw[->,>=stealth] (uncipher) -- (1.8,-5);}

\onslide<1->{\node at (-4,-0.5) {\LARGE Alice};}
\onslide<1->{\node at (-4,-3.5) {\LARGE Bob};}
\end{tikzpicture}}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Forward secrecy}
\begin{itemize}[<+->]
\item We use a single pair of private/public keys during several sets of transactions
\end{itemize}
\begin{alertblock}<+->{What if the private key is compromised?}
    \begin{itemize}[<+->]
    \item An attacker can decrypt all previously performed transactions
    \end{itemize}
\end{alertblock}
\begin{itemize}[<+->]
\item Private keys used for encryption have to be renewed with each connection
    \begin{itemize}
    \item Not the ones used for authentication
    \end{itemize}
\item This way, if a key is compromised, previous transactions cannot be decrypted
\item The Diffie-Hellman key exchange (and variants) have this property
    \begin{itemize}
    \item Each actor combines his private key with the public key of his correspondent
    \item Safely produces a common secret used to generate keys
    \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Illustration: DHKE}
\begin{center}
\scalebox{.4}{
\begin{tikzpicture}
\onslide<1->{\node at (-4,1.5) {\LARGE Alice};}
\onslide<1->{\node at (-4,-1.5) {\LARGE Bob};}
\onslide<1->{\draw[very thick] (-5,0) -- (0.5,0);}

\begin{scope}[shift={(-5,3)}, scale=.25]
\onslide<2->{\labelledkey{Green}{Public session key of Alice}}
\end{scope}

\begin{scope}[shift={(-5,-3)}, scale=.25]
\onslide<4->{\labelledkey{Green}{Public session key of Bob}}
\end{scope}

\onslide<6->{\node[draw, fill=yellow!50, minimum height=1cm, minimum width=3cm] (ca) at (3,3) {Combine};}
\onslide<11->{\node[draw, fill=yellow!50, minimum height=1cm, minimum width=3cm] (cb) at (3,-3) {Combine};}

\onslide<3->{\draw[->, >=stealth, thick, dotted] (-3,3) to[out=0,in=180] node[pos=.2, above, sloped] {\textbf{Send}} (cb.west);}
\onslide<5->{\draw[->, >=stealth, thick, dotted] (-3,-3) to[out=0,in=180] node[pos=.2, below, sloped] {\textbf{Send}} (ca.west);}

\begin{scope}[shift={(3,5)}, scale=.25]
\onslide<7->{\labelledkey{red!80}{Private session key of Alice}}
\end{scope}

\begin{scope}[shift={(3,-5.5)}, scale=.25]
\onslide<12->{\labelledkey{red!80}{Private session key of Bob}}
\end{scope}

\onslide<8->{\node[draw, fill=blue!40, minimum height=1cm, text width=4cm, align=center] (ci) at (3,0) {Public common piece of information};}
\onslide<8->{\draw[->,>=stealth, thick] (ci) -- (ca);}
\onslide<13->{\draw[->,>=stealth, thick] (ci) -- (cb);}

\onslide<7->{\draw[->,>=stealth, thick] (3,4.5) -- (ca);}
\onslide<12->{\draw[->,>=stealth, thick] (3,-4.5) -- (cb);}

\onslide<10->{\node[draw, fill=Dandelion, minimum height=1cm, text width=3cm, align=center] (cs) at (9,0) {Common secret};}
\onslide<9->{\draw[->,>=stealth, thick, dotted] (ca) to[out=0,in=90] (cs.north);}
\onslide<14->{\draw[->,>=stealth, thick, dotted] (cb) to[out=0,in=270] (cs.south);}

\onslide<19->{\node[draw, fill=yellow!50, minimum height=1cm, text width=4cm, align=center] (kdf) at (15,0) {Key derivation function};}
\onslide<18->{\draw[->,>=stealth, thick] (cs) -- (kdf);}

\onslide<20->{\draw[->,>=stealth, thick] (kdf) -- (19,0);}

\begin{scope}[shift={(20,0)}, scale=.25]
\onslide<21->{\labelledkey{Dandelion}{Secret session key}}
\end{scope}
\end{tikzpicture}}
\end{center}
\begin{itemize}
\onslide<15->{\item The ``combine'' algorithm is chosen in such a way that it has the \emph{same} output when executed with }
    \begin{itemize}
    \onslide<16->{\item the public key of Bob and the private key of Alice,}
    \onslide<17->{\item the private key of Bob and the public key of Alice.}
    \end{itemize}
\end{itemize}
\end{frame}

\subsection{Certificates and PKI}

\begin{frame}
\frametitle{Certificate}
\begin{itemize}[<+->]
\item Electronic document proving the ownership of a public key
    \begin{itemize}
    \item Issued/revoked/renewed by a certification authority (CA) according to the rules of a PKI
    \item A registration authority checks the identity of the owner
    \end{itemize}
\item It is assumed involved actors \emph{both} trust the CA
    \begin{itemize}
    \item With the help of a chain of trust
    \item Act as a trusted third party intermediated
    \end{itemize}
%\item Widely used for first-time key exchange
%    \begin{itemize}
%    \item Especially with Https (and TLS)
%    \end{itemize}
\item Contains
	\begin{itemize}
	\item information about the public key
	\item information about the owner's identification
	\item numerical signature of the issuer
    \item serial number, period of validity, etc.
	\end{itemize}
\item The CA makes sure information stored in the certificate are correct, and signs it
    \begin{itemize}
    \item Usually, a user crafts a CSR, signs it, and sends it to his CA in order to craft a certificate
    \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Chain of trust}
\begin{itemize}[<+->]
\item If is difficult, on practice, to have only a few CA
    \begin{itemize}
    \item Lots of different CA, with different price ranges, different requirements, etc.
    \end{itemize}
\end{itemize}
\begin{exampleblock}<+->{Question}
    \begin{itemize}[<+->]
    \item How can a certificate issued by some CA recognised by Apple can be trusted on somebody running a Microsoft software?
    \end{itemize}
\end{exampleblock}
\begin{block}<+->{Answer}
    \begin{itemize}[<+->]
    \item Apple and Microsoft and a third party intermediate that they both trust
    \end{itemize}
\end{block}
\begin{itemize}[<+->]
\item That CA signs both Microsoft and Apple certificates
\item Such ``high level'' CAs are called \emph{root} CAs
    \begin{itemize}
    \item Web browsers pack up lists of root CAs they trust
    \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Illustration}
\begin{center}
\scalebox{.6}{
\begin{tikzpicture}
\onslide<3->{\draw[rounded corners=5pt, thick] (0,0) rectangle (5,1.5);}
\onslide<3->{
\foreach \i in {1,2,...,5}
    \node[draw, minimum height=0.5cm, minimum width=0.5cm, inner sep=0pt, label={above:\footnotesize RCA\i}] (CA\i) at (\i-0.5, 0.5) {};
\node[anchor=west] at (0,2) {\textbf{Trusted Root CAs}};
}

\onslide<1->{\node (user) at (-1,-8) {\includesvg[width=0.75cm]{pics/user.svg}};}
\onslide<2->{\draw[thick, ->, >=stealth] (user) -- node[pos=.5, above, sloped] {trusts} (-1,0.75) -- (-0.1,0.75);}

\onslide<5->{\node (web) at (16,-8) {\textcolor{blue!80}{\texttt{www.mywebsite.com}}};}
\onslide<4->{\draw[thick, ->, >=stealth] (user) -- (web) node[midway, below] {connects};}
\onslide<7->{\node[draw] (webcert) at (16,-6) {Certificate : \texttt{mywebsite.com}};}
\onslide<6->{\draw[->, >=stealth, Green] (web) -- (webcert) node[midway, right] {has certificate};}

\onslide<9->{\node[draw] (CA12) at (10,-6) {CA12};}
\onslide<11->{\node[draw] (CA12cert) at (10,-4) {Certificate : CA12};}
\onslide<10->{\draw[->, >=stealth, Green] (CA12) -- (CA12cert) node[midway, right] {has certificate};}
\onslide<8->{\draw[->, >=stealth, Green] (webcert) -- (CA12) node[midway, above] {references};}

\onslide<13->{\node[draw] (CA27) at (5,-4) {CA27};}
\onslide<15->{\node[draw] (CA27cert) at (5,-2) {Certificate : CA27};}
\onslide<12->{\draw[->, >=stealth, Green] (CA12cert) -- (CA27) node[midway, above] {references};}
\onslide<14->{\draw[->, >=stealth, Green] (CA27) -- (CA27cert) node[midway, right] {has certificate};}

\onslide<16->{\draw[->, >=stealth, Green] (CA27cert) -- (CA2) node[midway, right, xshift=5pt] {references};}

\onslide<17->{\draw[->, red!80] (CA2) |- (CA27cert) node[pos=.5, left] {signs};}
\onslide<18->{\draw[->, red!80] (CA27) to[out=330,in=210] node[pos=.5, below] {signs} (CA12cert);}
\onslide<19->{\draw[->, red!80] (CA12) to[out=330,in=210] node[pos=.5, below] {signs} (webcert);}
\end{tikzpicture}}
\end{center}
\end{frame}

\section{Some applications}

\begin{frame}
\frametitle{Overview}
\begin{itemize}[<+->]
\item Cryptographic primitives are widely used in cybersecurity
    \begin{itemize}
    \item Security relies on their properties
    \item Hard to invert a hash function, decipher without the private key, etc.
    \end{itemize}
\item We overview a few of them and emphasise on the properties the security system has
    \begin{itemize}
    \item TLS: widely used in tunnel-requiring connections (Https, VPNs, RADIUS, etc.)
    \item Sequences of items: checking they cannot be forged, and that the sequence has integrity
    \item End to end encryption: how to store and transmit to servers that you cannot trust
    \end{itemize}
\end{itemize}
\end{frame}

\subsection{TLS}

\begin{frame}
\frametitle{Objectives}
\begin{itemize}[<+->]
\item When you connect to a server, you want
    \begin{itemize}
    \item to actually connect to the actual server,
    \item the privacy and integrity of exchanged data.
    \end{itemize}
%\item Extension of HTTP, over TLS
\item Protection against MITM attacks, replay attacks, etc.
\item Bidirectional encryption between client and server
\item Enabled with certificates
    \begin{itemize}
    \item The server is authenticated
    \end{itemize}
\item The first step of the connection is to perform a TLS handshake
\end{itemize}
\begin{exampleblock}<+->{Objective}
    \begin{itemize}[<+->]
    \item Securely generate and share a session key using DHKE
    \item That key will be used to encrypt data in both ways
    \end{itemize}
\end{exampleblock}
\end{frame}

\begin{frame}
\frametitle{Step-by-step in TLS 1.3}
\begin{enumerate}[<+->]
\item Client Hello
    \begin{itemize}
    \item The client generates a random key pair for a given algorithm, and sends a list of supported ciphers, a random nounce, and the generated public key
    \end{itemize}
\item Server Hello
    \begin{itemize}
    \item If the server supports the algorithm the client guessed, it generates a key pair, and sends the public key it generated and a random nounce (both signed), and its certificate
    \end{itemize}
\item Client Finish
    \begin{enumerate}
    \item The client checks the certificate with its CA, and the signature
    \item The client generates a pre-master secret, encrypt it with the server public key, and sends it
    \item The client generates a session key with DH
    \end{enumerate}
\item Server Finish
    \begin{enumerate}
    \item The server receives the pre-master secret from the client, and decrypts it
    \item The server generates a session key with DH
    \end{enumerate}
\end{enumerate}
\end{frame}

\subsection{End to end encryption}

\begin{frame}
\frametitle{Why}
\begin{itemize}[<+->]
\item On tunnel-relying protocols, data is automatically encrypted and decrypted by clients \emph{and} servers
    \begin{itemize}
    \item Such as with TLS, VPNs, etc.
    \end{itemize}
\item Servers have the ability to record and monitor everything
    \begin{itemize}
    \item ... which doesn't mean they do
    \end{itemize}
\item The same ``problem'' happens with database autociphering
    \begin{itemize}
    \item Entries are automatically encrypted / decrypted
    \end{itemize}
\item Protocols usually do not guarantee the confidentiality of metadata
\item If you can't trust a server, you need a solution
    \begin{itemize}
    \item For instance, to store sensitive informations on a server
    \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Principle}
\begin{exampleblock}<+->{End-to-end encryption}
    \begin{itemize}[<+->]
    \item Data is encrypted on client-side, by the sender's device
    \item Cryptographic material is also generated on client-side
        \begin{itemize}
        \item ... but can still be stored \emph{securely} on server side
        \end{itemize}
    \end{itemize}
\end{exampleblock}
\begin{itemize}[<+->]
\item Since cryptographic material is generated on client-side, the server doesn't have access to it
    \begin{itemize}
    \item The server cannot use your key to decipher
    \end{itemize}
\item Since data is encrypted on client-side, the server never has it plain
\item Nobody except the sender has the ability to decrypt
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Example: file storage with a keypair}
\begin{center}
\scalebox{.8}{
\begin{tikzpicture}
\onslide<3->{\node[draw, minimum width=2cm, fill=yellow!50] (keygen) at (0,0) {Key generation};}

\begin{scope}[shift={(3.5,0.125)}, scale=.25]
\onslide<5->{\labelledkey{Dandelion}{Secret key}}
\end{scope}
\onslide<4->{\draw[->, >= stealth] (keygen) -- (2.5,0);}

\begin{scope}[shift={(1,-2)}, scale=.8]
\onslide<2->{\file{white}{\small File}}
\end{scope}

\onslide<8->{\node[draw, minimum width=2cm, fill=yellow!50] (cipherf) at (3.65,-2) {Cipher}; }
\onslide<6->{\draw[->, >= stealth] (3.65, -0.5) -- (cipherf);}
\onslide<7->{\draw[->, >= stealth] (1.5,-2) -- (cipherf);}
\onslide<9->{\draw[->, >= stealth, dotted, thick] (cipherf) -- (8,-2) node[above, midway] {Send};}

\begin{scope}[shift={(7,2.125)}, scale=.25]
\onslide<10->{\labelledkey{Green}{Client's public key}}
\end{scope}

\onslide<13->{\node[draw, minimum width=2cm, fill=yellow!50] (cipherk) at (7.5,0) {Cipher};}
\onslide<12->{\draw[->, >= stealth] (7.5,1.5) -- (cipherk);}
\onslide<11->{\draw[->, >= stealth] (5,0) -- (cipherk);}

\onslide<14->{\draw[->, >= stealth, dotted, thick] (cipherk) -- (12,0) node[above, midway] {Send};}

\end{tikzpicture}}
\end{center}
\begin{itemize}
\onslide<1->{\item These steps happen on client side}
\onslide<15->{\item The server can't decipher, because it doesn't have the private key}
\end{itemize}
\end{frame}

\subsection{Threshold cryptography}

\begin{frame}
\frametitle{Threshold cryptography}
\begin{itemize}[<+->]
\item Idea: we want to share a secret
	\begin{itemize}
	\item Cryptographic key for ciphering or signing
	\item Access codes
	\end{itemize}
\item Motivation: allow service if at least $x$ people agree to cooperate-
	\begin{itemize}
	\item Bank safe, missile codes, etc.
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Overview}
\begin{exampleblock}<+->{Basic idea}
	\begin{itemize}[<+->]
	\item We want to share a secret (a key) between several people
	\item We want to be able to recover the secret if \emph{enough} of these people cooperate
	\item If an insufficient number of people is gathered, the secret is safe
	\end{itemize}
\end{exampleblock}
\begin{itemize}[<+->]
\item How to proceed ?!
	\begin{itemize}
	\item Several (mathematical) techniques
	\item Here: polynomial interpolation
	\end{itemize}
\item Motivation
	\begin{itemize}
	\item Fight against corruption of actors
	\item Force several people to cooperate to make important decisions
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{$(k,n)$ threshold cryptography}
\begin{exampleblock}<+->{Definition}
	\begin{itemize}[<+->]
	\item A cryptographic system is called a \emph{threshold cryptographic system} if several people (more than the threshold) need to cooperate in the protocol used to (de)cipher a message.
	\item Such a system is a $(k,n)$-threshold if at least $k$ people among $n$ must cooperate to (de)cipher the message
	\end{itemize}
\end{exampleblock}
\begin{itemize}[<+->]
\item Idea: the private key is shared among actors
\item Independently discovered by Shamir (polynomials) and Blakley (hyperplanes) in 1979
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Key sharing (1/2)}
\begin{exampleblock}<+->{Safe sharing}
	\begin{itemize}
	\item \emph{Safe sharing} if someone with less than $k$ pieces as no more information that someone with no pieces.
	\end{itemize}
\end{exampleblock}
\begin{itemize}[<+->]
\item Example: secret \texttt{Password} to share
	\begin{enumerate}
	\item People 1: \texttt{Pa......}
	\item People 2: \texttt{..ss....}
	\item People 3: \texttt{....wo..}
	\item People 4: \texttt{......rd}
	\end{enumerate}
\item Someone with no pieces must guess among $26^8 \simeq $ 208 billions possibilities
\item Someone with one piece must guess among $26^6 \simeq $ 308 millions possibilities
\item Unsafe sharing
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Key sharing (2/2)}
\begin{itemize}[<+->]
\item A secret is iteratively ciphered with $k$ public keys
\item The $k$ corresponding private keys are distributed among $n = k$ actors
	\begin{itemize}
	\item Someone can remove the first level of cipher
	\item If two people cooperate, they can remove two levels
	\item Unless $k$ persons cooperate, the secret is still completely confidential
	\end{itemize}
\item Bypassing the system without $k$ keys is reduced to decipher datas without a key
	\begin{itemize}
	\item Not very practical, but safe
	\end{itemize}
\end{itemize}
\begin{alertblock}<+->{Problem}
	\begin{itemize}[<+->]
	\item How to proceed so that $k < n$ people are able to decipher ?
	\end{itemize}
\end{alertblock}
\end{frame}

\begin{frame}
\frametitle{Example with few people (Blakley)}
\begin{center}
\includegraphics[width=6cm]{pics/secret.pdf}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Polynomial interpolation}
\begin{itemize}[<+->]
\item In the case of Shamir secret sharing, we rely on polynomial interpolation
\item To share a secret among $k$ people,
	\begin{enumerate}
	\item we work with a polynomial of degree $(k-1)$
	\item The key is one of the coefficient (arbitrary)
	\item Other coefficients are generated at random
	\item We generate $n$ distinct points of the polynomial and give one of them to each actor
	\end{enumerate}
\item If $k$ actors cooperate, we have $k$ points of a polynomial of degree $k - 1$
	\begin{enumerate}
	\item We build it with polynomial interpolation
	\item We recover the key
	\end{enumerate}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{In practice}
\begin{itemize}[<+->]
\item We have $n+1$ points $p_i = (x_i, y_i)$
\item We want to compute the coefficients of a polynomial
	\begin{align*}
	P(x) &= \displaystyle \sum_{i=0}^n a_i x^i\\
		 &= a_n x^n + a_{n-1} x^{n-1} + \dots + a_2 x^2 + a_1 x + a_0
	\end{align*}
\item Substituting $x$ in this equation and $P(x)$ by their values, we get a system with $n+1$ equations and $n+1$ unknown (the coefficients)
	\begin{equation*}
	\begin{bmatrix}
	x_0^n  & x_0^{n-1} & x_0^{n-2} & \ldots & x_0 & 1 \\
	x_1^n  & x_1^{n-1} & x_1^{n-2} & \ldots & x_1 & 1 \\
	\vdots & \vdots    & \vdots    & \ddots & \vdots & \vdots \\
	x_n^n  & x_n^{n-1} & x_n^{n-2} & \ldots & x_n & 1
	\end{bmatrix}
	\begin{bmatrix} a_n \\ a_{n-1} \\ \vdots \\ a_0 \end{bmatrix}  =
	\begin{bmatrix} y_0 \\ y_1 \\ \vdots \\ y_n \end{bmatrix}
	\end{equation*}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{System solving in practice}
\begin{itemize}[<+->]
\item We must solve this system in the variables $a_i$
\item Mimic the "manual" method
	\begin{itemize}
	\item Make the matrix triangular
	\end{itemize}
\item Gaussian method
	\begin{itemize}
	\item Running time: $\bigo(n^3)$
	\item High conditioning
		\begin{itemize}
		\item Subject to numerical errors: numerical instability
		\item How a controlled error on inputs alters the output ?
		\end{itemize}
	\end{itemize}
\item Other methods
	\begin{itemize}
	\item Rewrite the polynomial
		\begin{itemize}
		\item Lagrangian interpolation
		\item Newtonian interpolation
		\end{itemize}
	\item Divided differences
	\end{itemize}
\item Use Lapack\footnote{\url{https://netlib.org/lapack/}} for anything related to linear algebra
\end{itemize}
\end{frame}

\end{document}
