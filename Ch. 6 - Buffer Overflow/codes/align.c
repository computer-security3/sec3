#include "stdio.h"

struct A {};
struct B { char i; };
struct C { int i; };
struct D { char i; int j; };


int main()
{
    printf("%zu\n", sizeof(struct A));
    printf("%zu\n", sizeof(struct B));
    printf("%zu\n", sizeof(struct C));
    printf("%zu\n", sizeof(struct D));
}
