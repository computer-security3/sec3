#include <stdio.h>

void foo(int* pti)
{
	printf("Address of param    : %p\n", &pti);
}

int main()
{
	int x = 42;

	printf("Address of x        : %p\n", &x);
	printf("Address of function : %p\n", foo);
	foo(&x);	
}
