\input{../common/header.tex}
\input{../common/cmds.tex}
\input{../common/crypto.tex}

\title{Ch. 3 - Data security}

\begin{document}
\input{../common/front.tex}

\section{Introduction}

\begin{frame}
\frametitle{Hackerman}
\begin{exampleblock}<+->{Question}
	\begin{itemize}[<+->]
	\item How to bypass a security system without the proper codes ?
        \begin{itemize}
        \item Cryptographic system : without the keys
        \end{itemize}
	\end{itemize}
\end{exampleblock}
\begin{block}<+->{Part of an answer}
	\begin{itemize}[<+->]
	\item Exploit hardware construction, the implementation of the algorithms, or the serialisation process
	\end{itemize}
\end{block}
\begin{itemize}[<+->]
\item Several main techniques
	\begin{enumerate}
	\item Trial and fail
	\item Technical characteristics analysis
    \item Targeted forgery
	\end{enumerate}
\item Here, we consider the attacker has a (partial or complete) access to the system
	\begin{itemize}
	\item Physical access, main memory, database tables, etc.
	\end{itemize}
\end{itemize}
\end{frame}

\section{Physical attacks}

\begin{frame}
\frametitle{Analysis}
\begin{itemize}[<+->]
\item When they are running, cipher algorithms perform several operations
\item Some of these operations have technical consequences
	\begin{enumerate}
	\item Memory storage
	\item Running time
	\item Electrical consumption
	\item Electromagnetic radiation
	\item Noise
	\item Data traffic
	\end{enumerate}
\item Analysing these ``side effects'' can disclose information
    \begin{itemize}
    \item Side-channel attacks
    \item Can be very complicated
    \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Principle}
\begin{itemize}[<+->]
\item Instead of attacking an algorithm directly, we attack its implementation
	\begin{itemize}
	\item Hardware : the way the architecture running an algorithm is built
	\item Software : the way a running algorithm is implemented
	\end{itemize}
\item Idea : deduce sensitive information from observations
	\begin{itemize}
	\item Variations in consumption
	\item Variations in running time
	\item Behaviour from injections
	\end{itemize}
\item Based on technical experiments, trial and fail, etc.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Electric consumption attack}
\begin{itemize}
\onslide<1-> \item Monitor electrical inputs and outputs from processor pins
	\begin{itemize}
	\onslide<2-> \item With a electronic oscilloscope
	\end{itemize}
\onslide<3-> \item Analyse consumption variations
\onslide<4-> \item Some operations use \emph{more} power
	\begin{itemize}
	\onslide<5-> \item More cycles : more power
	\end{itemize}
\begin{center}
\visible<6->{\includegraphics[width=8cm]{pics/Power_attack.png}}
\end{center}
\onslide<7-> \item Very hard to detect (passive) and counter
	\begin{itemize}
	\onslide<8-> \item Build and implement ``correctly''
		\begin{itemize}
		\onslide<9-> \item So that power variation does not disclose information
		\end{itemize}
	\onslide<10-> \item Reduce electric consumption
		\begin{itemize}
		\onslide<11-> \item Harder to analyse
		\end{itemize}
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Electromagnetic radiation analysis}
\begin{itemize}[<+->]
\item Same principle as electrical attacks
\item Analyse EM radiation
\item Each instruction has a unique associated radiation
\item Monitor while ciphering / unciphering
\item Build up models
\item Local search of the key
\item Solution
	\begin{itemize}
	\item Faraday plating
	\item Algorithmic randomisation
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Montgomerry powering ladder}
\begin{itemize}[<+->]
\item ``Programmation technique'' allowing to fight some physical attacks
\end{itemize}
\begin{exampleblock}<+->{Principle}
	\begin{itemize}[<+->]
	\item Insert additional instructions to cover your tracks
	\item Idea: the same number of instructions, in the same order, must be executed when branching if/else and loops
	\end{itemize}
\end{exampleblock}
\begin{itemize}[<+->]
\item If the same instructions are executed, whatever the branching of the algorithm, then electrical consumption and electromagnetic radiation remain the same
\item Drawback: slight performance cost
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Naive modular exponentiation}
\begin{itemize}[<+->]
\item Used by RSA, DHKE, etc.
\end{itemize}
\begin{algorithmic}[1]
\REQUIRE $b,e,m \in \IN$
\ENSURE $b^e \mod m$
\IF{$m = 1$}
	\RETURN $0$
\ENDIF
\STATE $r \leftarrow 1$
\FOR {$1 \leqslant i \leqslant e$}
	\STATE $r \leftarrow (r \times b) \mod m$
\ENDFOR
\RETURN $r$
\end{algorithmic}
\begin{itemize}
\item Linear complexity
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Fast modular exponentiation}
\begin{algorithmic}[1]
\REQUIRE $b,e,m \in \IN$
\ENSURE $b^e \mod m$
\IF{$m = 1$}
	\RETURN $0$
\ENDIF
\STATE $r \leftarrow 1$
\WHILE{$e > 1$}
	\IF{$e \mod 2 = 1$}
		\STATE $r = (r \times b) \mod m$
	\ENDIF
	\STATE $e \leftarrow e \DIV 2$
	\STATE $b \leftarrow (b \times b) \mod m$
\ENDWHILE
\RETURN $r$
\end{algorithmic}
\begin{itemize}
\item $\bigo(log_2(e))$.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Safe fast modular exponentiation}
\begin{algorithmic}[1]
\REQUIRE $b,e,m \in \IN$
\ENSURE $b^e \mod m$
\IF{$m = 1$}
	\RETURN $0$
\ENDIF
\STATE $r \leftarrow 1$
\WHILE{$e > 1$}
	\STATE $x \leftarrow (r \times b) \mod m$
	\STATE $y \leftarrow r$
	\IF{$e \mod 2 = 1$}
		\STATE $r \leftarrow x$
	\ELSE
		\STATE $r \leftarrow y$
	\ENDIF
	\STATE $e \leftarrow e \DIV 2$
	\STATE $b \leftarrow (b \times b) \mod m$
\ENDWHILE
\RETURN $r$
\end{algorithmic}
\end{frame}

\section{Remanence attacks}

\begin{frame}
\frametitle{Idea}
\begin{itemize}[<+->]
\item Exploit the fact that memory is permanent
	\begin{itemize}
	\item Variables stored in memory, unused, deallocated
	\item Data stored on hard drives
	\end{itemize}
\item Different types of threat
	\begin{itemize}
	\item Find the address of a variable / function
		\begin{itemize}
		\item Change the value in memory
		\item Change the return address
		\end{itemize}
	\item Find sensitive information in memory or the drives
	\end{itemize}
\item Mandatory to
	\begin{itemize}
	\item erase memory
	\item check integrity
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Protection}
\begin{itemize}[<+->]
\item Four examples in \texttt{C++}
	\begin{itemize}
	\item Find the address of some variable
	\item Change the content of the variable through that address
	\end{itemize}
\end{itemize}
\begin{exampleblock}<+->{Question}
	\begin{itemize}[<+->]
	\item How to protect against this ?
	\end{itemize}
\end{exampleblock}
\begin{block}<+->{Part of an answer}
	\begin{itemize}[<+->]
	\item Desktop
		\begin{itemize}
		\item Canaries
		\item Compiler-induced obfuscation
		\end{itemize}
	\item Client
		\begin{itemize}
		\item Check integrity on the server side
		\end{itemize}
	\end{itemize}
\end{block}
\end{frame}

\begin{frame}
\frametitle{Canaries}
\begin{exampleblock}<+->{Designing canaries}
	\begin{itemize}[<+->]
	\item Canaries should not be overflown
	\item The address of canaries should not be found
	\end{itemize}
\end{exampleblock}
\begin{itemize}[<+->]
\item Usually, canaries are random and placed in the data segment
	\begin{itemize}
	\item They cannot be overflown from \texttt{ss}
	\item Their address cannot be easily found with \texttt{memscan}
	\end{itemize}
\item Stackguard is a popular technique allowing the transparent implementation of canaries to protect the stack against buffer overflow
	\begin{itemize}
	\item Activated by default in \texttt{gcc}
	\item More details in Ch. 6
	\end{itemize}
\item Sometimes, manual implementation of canaries is necessary to protect dedicated variables
\end{itemize}
\end{frame}

\section{Serialisation}

\begin{frame}
\frametitle{The context}
\begin{itemize}[<+->]
\item Often, we need to transmit or store objects with attributes
\item The serialisation mechanism of the programming language allows to convert
    \begin{itemize}
    \item objects to bytes
    \item bytes to objects
    \end{itemize}
\item Sensitive objects are often stored ciphered
\item No protection against integrity and replay whatsoever
    \begin{itemize}
    \item Some padding schemes provide a form of integrity guarantees (GCM)
    \end{itemize}
\item OWASP Top 10 A08
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Stating the obvious}
\begin{itemize}[<+->]
\item Sensitive data is not transmitted or stored as plain text
\item Always piped through a TLS tunnel
\item Always stored ciphered
    \begin{itemize}
    \item In end-to-end encryption contexts, this operation is performed on client-side
%    \item Some padding schemes provide integrity (GCM)
    \end{itemize}
\item Maybe we need more
    \begin{itemize}
    \item Stronger guarantees on integrity and non-repudiation
    \item Detect deleted entries
    \end{itemize}
\item Administrators could be compromised
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Transmission and storage of ciphered data}
\begin{itemize}[<+->]
\item We could ``simply'' send/store encrypted data
\end{itemize}
\begin{alertblock}<+->{Problem}
    \begin{itemize}[<+->]
    \item It can be tampered with
    \item No concept of sequences
    \item No protection against replay attacks
    \end{itemize}
\end{alertblock}
\begin{itemize}[<+->]
\item We want guarantees both in the standard client/server architecture, and in end-to-end encryption contexts
\item We want similar guarantees with logs
    \begin{itemize}
    \item Administrators can falsify them
    \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Signing: the natural solution}
\begin{itemize}[<+->]
\item We could sign everything
\end{itemize}
\begin{alertblock}<+->{Problem}
    \begin{enumerate}[<+->]
    \item Requires a keypair
        \begin{itemize}
        \item In non end-to-end encryption contexts, the client usually doesn't have one
        \end{itemize}
    \item Slower than a simple hash
    \end{enumerate}
\end{alertblock}
\begin{itemize}[<+->]
\item Could still be acceptable tough
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Authenticated encryption and associated data}
\begin{itemize}[<+->]
\item On SSL/TLS, HMACs are used
\item A sequence number is added to each message
    \begin{itemize}
    \item Protects against deletion and replay
    \end{itemize}
\item An integrity-checking message authentication code (HMAC) is added, computed from
    \begin{itemize}
    \item the sequence number
    \item the message (ciphered or not)
    \item the session key (TLS, or app-maintained)
    \end{itemize}
\item The HMAC is computed with a hash function
\item Several different schemes
    \begin{itemize}
    \item Encrypt then MAC
    \item Encrypt and MAC
    \item MAC then Encrypt
    \end{itemize}
\end{itemize}
\end{frame}

\definecolor{darkred}{RGB}{180,0,0}

\begin{frame}
\frametitle{Illustration}
\begin{itemize}
\onslide<1->{\item Assumes you have a session key}
\onslide<2->{\item Scheme example: Mac then Encrypt with sequence number}
\end{itemize}
\begin{center}
\scalebox{.6}{
\begin{tikzpicture}
\begin{scope}[shift={(3,4)},scale=.25]
\onslide<7->{\labelledkey{Dandelion}{Secret key}}
\end{scope}
\onslide<3->{\node[draw,fill=blue!30, minimum width= 3cm] (plain0) at (0,6) {``Hello Bob!''};}
\onslide<5->{\node[draw,fill=blue!30, minimum width= 3cm] (plain) at (0,4) {``Hello Bob!''};}
\onslide<4->{\draw[->, >=stealth] (plain0) -- (plain); }
\onslide<5->{\node[draw,fill=darkred, minimum width= 0.5cm] (sn1) at (-1.75,4) {SN};}
\onslide<13->{\node[draw,fill=blue!30, minimum width= 3cm] (plain2) at (0,0) {``Hello Bob!''};}
\onslide<13->{\node[draw,fill=darkred, minimum width= 0.5cm] (sn2) at (-1.75,0) {SN};}
\onslide<11->{\node[draw, fill=Green, minimum width=1cm] (mac) at (2,0) {MAC};}

\onslide<9->{\node[draw, inner sep = 10pt,fill=yellow!50] (hash) at (2,2) {Keyed hash};}
\onslide<12->{\draw[->,>=stealth] (plain) -- (plain2);}
\onslide<6->{\draw[->,>=stealth] (plain) -- (0,3) -- (2,3) -- (hash);}
\onslide<6->{\draw[->,>=stealth] (sn1) -- (-1.75,3) -- (2,3) -- (hash);}
\onslide<10->{\draw[->,>=stealth] (hash) -- (mac);}

\onslide<16->{\node[draw, inner sep = 10pt,fill=yellow!50] (cipher) at (1,-2) {Cipher};}
\onslide<15->{\draw[->,>=stealth] (4,3.5) -- (4,-2) -- (cipher);}

\onslide<8->{\draw[->,>=stealth] (4,3.5) -- (4,2) -- (hash);}

\onslide<14->{\draw[->,>=stealth] (plain2) -- (0,-1) -- (1,-1) -- (cipher); }
\onslide<14->{\draw[->,>=stealth] (sn2) -- (-1.75,-1) -- (1,-1) -- (cipher); }
\onslide<14->{\draw[->,>=stealth] (mac) -- (2,-1) -- (1,-1) -- (cipher);}
\onslide<17->{\draw[->,>=stealth] (cipher) -- (1,-3) node[below] {Output};}
\end{tikzpicture}}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Features}
\begin{itemize}[<+->]
\item In this context, HMACs are used as a symmetric signature
    \begin{itemize}
    \item Both client and server sign in the same way
    \item Nobody without the session key can sign (non repudiation)
    \end{itemize}
\item Integrity is enforced by the hash function (or signature)
\item Deletions are detected with the sequence number
    \begin{itemize}
    \item ... and insertions with the HMAC (or signature)
    \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Blockchains}
\begin{itemize}[<+->]
\item In order to secure the integrity of sequences of items, the system can also use blockchains
\item Chain of ``blocks'', each block containing
    \begin{itemize}
    \item a sequence number
    \item data
    \item the MAC of the previous block
    \item the MAC of the current block
    \end{itemize}
\item Tampering with a block will require recomputing the hash of every subsequent block
\item Offers good protection if
    \begin{itemize}
%    \item The system is decentralised
    \item There are enough records per unit of time
    \item There is a form of consensus on who can craft a block
    \item There is a form of consensus on what is a legitimate block
    \end{itemize}
\item MACs are produced either
    \begin{itemize}
    \item by directly by hashing the payload
    \item by computing a HMAC
    \item by signing
    \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{The special case of logs}
\begin{itemize}[<+->]
\item The backend should record \emph{everything}
\end{itemize}
\begin{exampleblock}<+->{Two main types of entries}
    \begin{enumerate}[<+->]
    \item Standard system activity, for debugging purposes
    \item \emph{Everything} related to security
        \begin{itemize}
        \item Success and failure both with authentication and AC, data going in and out, etc.
        \end{itemize}
    \end{enumerate}
\end{exampleblock}
\begin{itemize}[<+->]
\item Security entries should be recorded in a \emph{separated} server
\item Different administrators on both servers
\end{itemize}
\begin{exampleblock}<+->{Objective}
    \begin{itemize}[<+->]
    \item Enforces separation of duties
    \end{itemize}
\end{exampleblock}
\end{frame}

\begin{frame}
\frametitle{Implementation}
\begin{itemize}[<+->]
\item Unciphered logs shouldn't contain sensitive information
\item Even ciphered logs are sensitive
    \begin{itemize}
    \item A malicious administrator could temper with the system and erase its tracks
    \end{itemize}
\end{itemize}
\begin{exampleblock}<+->{Step by step}
    \begin{enumerate}[<+->]
    \item The backend crafts a security log entry
    \item The backend adds a sequence number, and signs
        \begin{itemize}
        \item Or crafts a HMAC, or a blockchain
        \end{itemize}
    \item The entry is sent to the server logging security
    \item The sequence number and signature are checked
    \item The security server signs and stores
        \begin{itemize}
        \item Or crafts a HMAC, or a blockchain
        \end{itemize}
    \end{enumerate}
\end{exampleblock}
\end{frame}

\end{document}
