#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

extern char** environ;

int main(int argc, char* argv[])
{
	if(argc < 2)
		fprintf(stderr, "Error\n");

	char * const args[2] = {"/usr/bin/env", NULL};
	char * const newenv[3] = {"AAA=aaa", "BBB=bbb", NULL};		
	
	int choice = atoi(argv[1]);
	if(choice == 1)
		execve(args[0], args, NULL); //we pass nothing
	else if(choice == 2)
		execve(args[0], args, newenv);
	else if(choice == 3)
		execve(args[0], args, environ);
	else
		printf("Error\n");
}
