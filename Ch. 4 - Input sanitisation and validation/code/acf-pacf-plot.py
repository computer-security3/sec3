import pandas
import matplotlib.pyplot as graph
from pandas.plotting import register_matplotlib_converters
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf

register_matplotlib_converters()

#read date and format it
ice_cream_prod = pandas.read_csv('ice-cream.csv')
ice_cream_prod.rename(columns={'DATE':'date', 'IPN31152N':'production'}, inplace=True)
ice_cream_prod['date'] = pandas.to_datetime(ice_cream_prod.date)
ice_cream_prod.set_index('date', inplace=True)
start_date = pandas.to_datetime('2010-01-01')
ice_cream_prod = ice_cream_prod[start_date:]

#print(ice_cream_prod.head()) #what does it look like

#plot data
graph.figure(figsize=(10,4))
graph.plot(ice_cream_prod.production)
graph.title('Ice Cream Production over Time', fontsize=20)
graph.ylabel('Production', fontsize=16)
for year in range(2011,2021):
    graph.axvline(pandas.to_datetime(str(year)+'-01-01'), color='k', linestyle='--', alpha=0.2)
graph.show()
#Q: what the heck happended in 2015?

#plot ACF
acf_graph = plot_acf(ice_cream_prod.production, lags=100)
graph.show()
#ACF decreasing is to be expected

#plot PACF
pacf_graph = plot_pacf(ice_cream_prod.production)
graph.show()
#PACF = 1 with lag 0 is trivial: correlation with itself
#significance is large with lags 1, 2, 3, 10, 13