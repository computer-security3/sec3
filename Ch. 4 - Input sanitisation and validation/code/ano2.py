import pandas
import numpy
import matplotlib.pyplot as graph
from datetime import datetime
from datetime import timedelta
from pandas.plotting import register_matplotlib_converters
from statsmodels.tsa.stattools import acf, pacf
from statsmodels.tsa.arima.model import ARIMA
from time import time
import sys

register_matplotlib_converters()

def parse_date(s):
    return datetime.strptime(s, '%Y-%m-%d')

def find_anomaly(dataset):
    min_dev = sys.maxsize
    anomaly = None
    for date in dataset.index:
        other_data = dataset[dataset.index != date]
        curr_dev = other_data.std()
        if curr_dev.production < min_dev:
            min_dev = curr_dev.production
            anomaly = date
    return min_dev, anomaly

ice_cream_prod = pandas.read_csv('ice-cream.csv')
ice_cream_prod.rename(columns={'DATE':'date', 'IPN31152N':'production'}, inplace=True)
ice_cream_prod['date'] = pandas.to_datetime(ice_cream_prod.date)
ice_cream_prod.set_index('date', inplace=True)

ice_cream_prod = ice_cream_prod.asfreq(pandas.infer_freq(ice_cream_prod.index))

start_date = pandas.to_datetime('2010-01-01')
ice_cream_prod = ice_cream_prod[start_date:]

graph.figure(figsize=(10,4))
graph.plot(ice_cream_prod.production)
graph.title('Ice Cream Production over Time', fontsize=20)
graph.ylabel('Production', fontsize=16)
for year in range(2010,2021):
    graph.axvline(pandas.to_datetime(str(year)+'-01-01'), color='k', linestyle='--', alpha=0.2)
graph.show()

#we're going to build up a model of the first 9 years, and try to predict the last year
training_end = pandas.to_datetime('2018-12-01')
testing_end = pandas.to_datetime('2019-12-01')
training_set = ice_cream_prod[:training_end]
testing_set = ice_cream_prod[training_end + timedelta(days=1):testing_end]

#build and fit the model
model = ARIMA(training_set, order=(3,0,3))
model = model.fit()

#test our model
pred_start_date = testing_set.index[0]
pred_end_date = testing_set.index[-1]

prediction = model.predict(start=pred_start_date, end=pred_end_date)
testing_set = testing_set.squeeze(); 

#deviation method
deviations = ice_cream_prod.copy()
for date in deviations.index:
    window = ice_cream_prod.loc[:date]    
    deviations.loc[date] = window.std()
    
diff_deviations = deviations.diff()
diff_deviations = diff_deviations.dropna()

graph.figure(figsize=(10,4))
graph.plot(diff_deviations)
graph.title('Deviation Differences', fontsize=20)
graph.ylabel('Production', fontsize=16)
for year in range(start_date.year,training_end.year):
    graph.axvline(pandas.to_datetime(str(year)+'-01-01'), color='k', linestyle='--', alpha=0.2)
graph.show()

#seasonal
month_deviations = ice_cream_prod.groupby(lambda d: d.month).std()
graph.figure(figsize=(10,4))
graph.plot(month_deviations)
graph.title('Deviation by Month', fontsize=20)
graph.ylabel('Production', fontsize=16)
graph.show()

april_data = ice_cream_prod[ice_cream_prod.index.month == 4]
may_data = ice_cream_prod[ice_cream_prod.index.month == 5]
june_data = ice_cream_prod[ice_cream_prod.index.month == 6]

april_ano = find_anomaly(april_data)
print("Anomaly in April:", april_ano[1], "with std. dev. of", april_ano[0])
may_ano = find_anomaly(may_data)
print("Anomaly in May:", may_ano[1], "with std. dev. of", may_ano[0])
june_ano = find_anomaly(june_data)
print("Anomaly in June:", june_ano[1], "with std. dev. of", june_ano[0])

#build up models from increasingly larger datasets to see how the anomaly impacts predictions
#predictions = testing_set.copy()
#for train_end in testing_set.index:
#    training_set = ice_cream_prod[:training_end - timedelta(days=1)]
#    model = ARIMA(training_set, order=(3,0,3))
#    model = model.fit()
#    pred = model.forecast()
#    predictions[training_end] = pred

#graph.figure(figsize=(10,4))
#graph.plot(testing_set)
#graph.plot(prediction, color="r")
#graph.title("Prediction from AR model", fontsize=20)
#graph.ylabel("Production", fontsize=16)
#graph.axhline(0, color="r", linestyle="--", alpha=0.2)
#for year in range(2019,2021):
#    graph.axvline(pandas.to_datetime(str(year)+'-01-01'), color='k', linestyle='--', alpha=0.2)
#graph.show()

#graph.figure(figsize=(10,4))
#graph.plot(error)
#graph.title("Error from AR model", fontsize=20)
#graph.ylabel("Error", fontsize=16)
#graph.axhline(0, color="r", linestyle="--", alpha=0.2)
#for year in range(2019,2021):
#    graph.axvline(pandas.to_datetime(str(year)+'-01-01'), color='k', linestyle='--', alpha=0.2)
#graph.show()
