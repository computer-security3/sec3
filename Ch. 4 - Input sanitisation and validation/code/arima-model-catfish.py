import pandas
import numpy
import matplotlib.pyplot as graph
from datetime import datetime
from datetime import timedelta
from pandas.plotting import register_matplotlib_converters
from statsmodels.tsa.stattools import acf, pacf, adfuller
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf
from statsmodels.tsa.arima.model import ARIMA
from time import time

register_matplotlib_converters()

def parse_date(s):
    return datetime.strptime(s, '%Y-%m-%d')

catfish_sales = pandas.read_csv('catfish.csv', parse_dates=[0], index_col=0, squeeze=True, date_parser=parse_date)
catfish_sales = catfish_sales.asfreq(pandas.infer_freq(catfish_sales.index))

start_date = datetime(2000,1,1)
end_date = datetime(2004,1,1)
catfish_sales = catfish_sales[start_date:end_date]

graph.figure(figsize=(10,4))
graph.plot(catfish_sales)
graph.title('Catfish Sales in 1000s of Pounds', fontsize=20)
graph.ylabel('Sales', fontsize=16)
for year in range(start_date.year,end_date.year):
    graph.axvline(pandas.to_datetime(str(year)+'-01-01'), color='k', linestyle='--', alpha=0.2)
graph.axhline(catfish_sales.mean(), color='r', alpha=0.2, linestyle='--')
graph.show()

diff_set = catfish_sales
non_stationary = True
diff_order = 0
while non_stationary:
    adf_test = adfuller(diff_set)
    pval = adf_test[1]
    print("p-value with d=", diff_order, ":", pval)
    if pval < 0.05:
        non_stationary = False
    else:
        diff_order+=1
        diff_set = diff_set.diff()[1:]
print("Differentiating order:", diff_order)

graph.figure(figsize=(10,4))
graph.plot(diff_set)
graph.title('Difference of Catfish Sales', fontsize=20)
graph.ylabel('Sales', fontsize=16)
for year in range(start_date.year,end_date.year):
    graph.axvline(pandas.to_datetime(str(year)+'-01-01'), color='k', linestyle='--', alpha=0.2)
graph.axhline(diff_set.mean(), color='r', alpha=0.2, linestyle='--')
graph.show()

acf_graph = plot_acf(diff_set, lags=15)
graph.show()

pacf_graph = plot_pacf(diff_set)
graph.show()

training_end = datetime(2003,7,1)
testing_end = datetime(2004,1,1)
training_set = catfish_sales[:training_end]
testing_set = catfish_sales[training_end + timedelta(days=1):testing_end]

#build and fit the model
model = ARIMA(training_set, order=(4,1,4))
start = time()
fitted_model = model.fit()
end = time()

print(fitted_model.summary())

#test our model
pred_start_date = testing_set.index[0]
pred_end_date = testing_set.index[-1]

prediction = fitted_model.predict(start=pred_start_date, end=pred_end_date)
testing_set = testing_set.squeeze(); 
error = (testing_set - prediction)**2 / (testing_set.mean() * prediction.mean())**0.5

graph.figure(figsize=(10,4))
graph.plot(testing_set)
graph.plot(prediction, color="r")
graph.title("Prediction from ARIMA model", fontsize=20)
graph.ylabel("Production", fontsize=16)
graph.axhline(0, color="r", linestyle="--", alpha=0.2)
graph.show()

graph.figure(figsize=(10,4))
graph.plot(error)
graph.title("Error from ARIMA model", fontsize=20)
graph.ylabel("Error", fontsize=16)
graph.axhline(0, color="r", linestyle="--", alpha=0.2)
graph.show()
