import pandas
import numpy
import matplotlib.pyplot as graph
from datetime import datetime
from datetime import timedelta
from pandas.plotting import register_matplotlib_converters
from statsmodels.tsa.stattools import acf, pacf, adfuller
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf
from statsmodels.tsa.arima.model import ARIMA
from time import time

register_matplotlib_converters()

def parse_date(s):
    return datetime.strptime(s, '%Y-%m-%d')

def differentiate(dataset, max_diff=10):
    diff_set = dataset
    non_stationary = True
    diff_order = 0
    while non_stationary and diff_order <= max_diff:
        adf_test = adfuller(diff_set)
        pval = adf_test[1]
        if pval < 0.05:
            non_stationary = False
        else:
            diff_order+=1
            diff_set = diff_set.diff()[1:]
    if diff_order  <= max_diff:
        return diff_order, diff_set
    else:
        return -1, []

catfish_sales = pandas.read_csv('catfish.csv', parse_dates=[0], index_col=0, squeeze=True, date_parser=parse_date)
catfish_sales = catfish_sales.asfreq(pandas.infer_freq(catfish_sales.index))

start_date = datetime(2000,1,1)
end_date = datetime(2004,1,1)
catfish_sales = catfish_sales[start_date:end_date]

diff_order, diff_set = differentiate(catfish_sales)

training_end = datetime(2003,1,1)
testing_end = datetime(2004,1,1)

#regular dataset
training_set = catfish_sales[:training_end]
testing_set = catfish_sales[training_end + timedelta(days=1):testing_end]
model = ARIMA(training_set, order=(4,1,4))
fitted_model = model.fit()
print(fitted_model.summary())

pred_start_date = testing_set.index[0]
pred_end_date = testing_set.index[-1]
prediction = fitted_model.predict(start=pred_start_date, end=pred_end_date)
testing_set = testing_set.squeeze(); 
error = (testing_set - prediction)**2 / (testing_set.mean() * prediction.mean())**0.5

#anomalous dataset
anomalous_sales = catfish_sales.copy()
anomalous_sales[datetime(2001,12,1)] = 10000 #very low
an_training_set = anomalous_sales[:training_end]
an_testing_set = anomalous_sales[training_end + timedelta(days=1):testing_end]
an_model = ARIMA(an_training_set, order=(4,1,4))
an_fitted_model = an_model.fit()
print(an_fitted_model.summary())

pred_start_date = an_testing_set.index[0]
pred_end_date = an_testing_set.index[-1]
an_prediction = an_fitted_model.predict(start=pred_start_date, end=pred_end_date)
an_testing_set = an_testing_set.squeeze(); 
an_error = (an_testing_set - an_prediction)**2 / (an_testing_set.mean() * an_prediction.mean())**0.5

#displays
graph.figure(figsize=(10,4))
graph.plot(catfish_sales)
graph.plot(anomalous_sales, color="green")
graph.plot(an_prediction, color="magenta")
graph.plot(prediction, color="red")
graph.legend(("Dataset","Anomalous dataset", "Predictions from regular model", "Predictions from anomalous model"), fontsize=16)
graph.title("Prediction from ARIMA model", fontsize=20)
graph.ylabel("Production", fontsize=16)
graph.axhline(0, color="r", linestyle="--", alpha=0.2)
for year in range(start_date.year,end_date.year):
    graph.axvline(pandas.to_datetime(str(year)+'-01-01'), color='k', linestyle='--', alpha=0.2)
graph.show()

graph.figure(figsize=(10,4))
graph.plot(error)
graph.plot(an_error, color="red")
graph.legend(("Error from the regular model", "Error from anomalous model"), fontsize=16)
graph.title("Error from ARIMA model", fontsize=20)
graph.ylabel("Error", fontsize=16)
graph.axhline(0, color="r", linestyle="--", alpha=0.2)
graph.show()

