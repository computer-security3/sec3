import pandas
import numpy
import matplotlib.pyplot as graph
from datetime import datetime
from datetime import timedelta
from pandas.plotting import register_matplotlib_converters
from statsmodels.tsa.stattools import acf, pacf, adfuller
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf
from statsmodels.tsa.arima.model import ARIMA
from time import time

register_matplotlib_converters()

def parse_date(s):
    return datetime.strptime(s, '%Y-%m-%d')

ice_cream_prod = pandas.read_csv('ice-cream.csv')
ice_cream_prod.rename(columns={'DATE':'date', 'IPN31152N':'production'}, inplace=True)
ice_cream_prod['date'] = pandas.to_datetime(ice_cream_prod.date)
ice_cream_prod.set_index('date', inplace=True)

ice_cream_prod = ice_cream_prod.asfreq(pandas.infer_freq(ice_cream_prod.index))

start_date = pandas.to_datetime('2010-01-01')
ice_cream_prod = ice_cream_prod[start_date:]

graph.figure(figsize=(10,4))
graph.plot(ice_cream_prod.production)
graph.title('Ice Cream Production over Time', fontsize=20)
graph.ylabel('Production', fontsize=16)
for year in range(2010,2021):
    graph.axvline(pandas.to_datetime(str(year)+'-01-01'), color='k', linestyle='--', alpha=0.2)
graph.show()

#we're going to build up a model of the first 9 years, and try to predict the last year
training_end = pandas.to_datetime('2018-12-01')
testing_end = pandas.to_datetime('2019-12-01')
training_set = ice_cream_prod[:training_end]
testing_set = ice_cream_prod[training_end + timedelta(days=1):testing_end]

diff_set = training_set
non_stationary = True
diff_order = 0
while non_stationary:
    adf_test = adfuller(diff_set.production.dropna())
    pval = adf_test[1]
    print("p-value with d=", diff_order, ":", pval)
    if pval < 0.05:
        non_stationary = False
    else:
        diff_order+=1
        diff_set = diff_set.diff()[1:]
print("Differentiating order:", diff_order)

acf_graph = plot_acf(diff_set.production, lags=100)
graph.show()

pacf_graph = plot_pacf(diff_set.production)
graph.show()

#build and fit the model
model = ARIMA(training_set, order=(3,0,3)) #p d q -> d = 0 and q = 0 means it's an AR model
start = time()
fitted_model = model.fit()
end = time()

print("Fitting time:", end - start) #the higher the order (and dataset size), the more time it takes
print(fitted_model.summary())

#test our model
pred_start_date = testing_set.index[0]
pred_end_date = testing_set.index[-1]

prediction = fitted_model.predict(start=pred_start_date, end=pred_end_date)
testing_set = testing_set.squeeze(); 
error = (testing_set - prediction)**2 / (testing_set.mean() * prediction.mean())**0.5

graph.figure(figsize=(10,4))
graph.plot(testing_set)
graph.plot(prediction, color="r")
graph.title("Prediction from AR model", fontsize=20)
graph.ylabel("Production", fontsize=16)
graph.axhline(0, color="r", linestyle="--", alpha=0.2)
for year in range(2019,2021):
    graph.axvline(pandas.to_datetime(str(year)+'-01-01'), color='k', linestyle='--', alpha=0.2)
graph.show()

graph.figure(figsize=(10,4))
graph.plot(error)
graph.title("Error from AR model", fontsize=20)
graph.ylabel("Error", fontsize=16)
graph.axhline(0, color="r", linestyle="--", alpha=0.2)
for year in range(2019,2021):
    graph.axvline(pandas.to_datetime(str(year)+'-01-01'), color='k', linestyle='--', alpha=0.2)
graph.show()


