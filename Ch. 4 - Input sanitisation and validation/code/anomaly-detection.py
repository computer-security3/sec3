import pandas
import numpy
import matplotlib.pyplot as graph
from datetime import datetime
from datetime import timedelta
from pandas.plotting import register_matplotlib_converters
from statsmodels.tsa.stattools import acf, pacf, adfuller
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf
from statsmodels.tsa.arima.model import ARIMA
from time import time

register_matplotlib_converters()

def parse_date(s):
    return datetime.strptime(s, '%Y-%m-%d')

def differentiate(dataset):
    diff_set = dataset.copy()
    non_stationary = True
    diff_order = 0
    while non_stationary:
        adf_test = adfuller(diff_set)
        pval = adf_test[1]
        if pval < 0.05:
            non_stationary = False
        else:
            diff_order+=1
            diff_set = diff_set.diff()[1:]
    return diff_order, diff_set

catfish_sales = pandas.read_csv('catfish.csv', parse_dates=[0], index_col=0, squeeze=True, date_parser=parse_date)
catfish_sales = catfish_sales.asfreq(pandas.infer_freq(catfish_sales.index))

start_date = datetime(1996,1,1)
end_date = datetime(2000,1,1)
catfish_sales = catfish_sales[start_date:end_date]

#artificially introduce an anomaly
catfish_sales[datetime(1998,12,1)] = 10000

graph.figure(figsize=(10,4))
graph.plot(catfish_sales)
graph.title('Anomalous Catfish Sales in 1000s of Pounds', fontsize=20)
graph.ylabel('Sales', fontsize=16)
for year in range(start_date.year,end_date.year):
    graph.axvline(pandas.to_datetime(str(year)+'-01-01'), color='k', linestyle='--', alpha=0.2)
graph.axhline(catfish_sales.mean(), color='r', alpha=0.2, linestyle='--')
graph.show()

diff_order, diff_set = differentiate(catfish_sales)
print(diff_order)

training_end = datetime(1999,7,1)
testing_end = datetime(2000,1,1)
training_set = catfish_sales[:training_end]
testing_set = catfish_sales[training_end + timedelta(days=1):testing_end]

#build up models from increasingly larger datasets to see how the anomaly impacts predictions
#predictions = testing_set.copy()
#for train_end in testing_set.index:
#    training_set = catfish_sales[:training_end - timedelta(days=1)]
#    model = ARIMA(training_set, order=(4,diff_order,4))
#    fitted_model = model.fit()

#    pred = fitted_model.forecast()
#    predictions[training_end] = pred
model = ARIMA(training_set, order=(4,1,4))
fitted_model = model.fit()

pred_start_date = testing_set.index[0]
pred_end_date = testing_set.index[-1]
prediction = fitted_model.predict(start=pred_start_date, end=pred_end_date)

graph.figure(figsize=(10,4))

graph.plot(catfish_sales)
graph.plot(prediction)
#print(anomalous_sales)
#print()
#print(predictions)

graph.legend(('Data', 'Predictions'), fontsize=16)

graph.title('Catfish Sales in 1000s of Pounds', fontsize=20)
graph.ylabel('Sales', fontsize=16)
for year in range(start_date.year,end_date.year):
    graph.axvline(pandas.to_datetime(str(year)+'-01-01'), color='k', linestyle='--', alpha=0.2)
graph.show()

deviations = pandas.Series(dtype=float, index = catfish_sales.index)
for date in deviations.index:   
    window = catfish_sales.loc[:date]    
    deviations.loc[date] = window.std()
    
#compute the difference in deviation between one time point and the next
diff_deviations = deviations.diff()
diff_deviations = deviations.dropna()

graph.figure(figsize=(10,4))
graph.plot(diff_deviations)
graph.title('Deviation Differences', fontsize=20)
graph.ylabel('Sales', fontsize=16)
for year in range(start_date.year,end_date.year):
    graph.axvline(pandas.to_datetime(str(year)+'-01-01'), color='k', linestyle='--', alpha=0.2)
graph.show()